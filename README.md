# bpsimaas-web
Web front-end for bpsimaas using vue v.2.x.

## Project setup

Usin npm

```
$ npm install
```

using yarn
```bash
$ yarn # install
```

### Compiles and hot-reloads for development and simple demos

Using npm

```
$ npm run serve
```

Using yarn

```
$ yarn run serve
```

### Compiles and minifies for production

Build and minifies content, to serve it as a static page, via npm

```bash
$ npm run build
```

or

```bash
$ yarn build
```

### Lints and fixes files
```
npm run lint
```

## Deployment

Should be easy using github pages.
