declare module "bpmn-js/lib/BaseModeler" {
    import BaseViewer from 'bpmn-js/lib/BaseViewer';
    /**
     * A base modeler for BPMN 2.0 diagrams.
     *
     * Have a look at {@link Modeler} for a bundle that includes actual features.
     *
     * @param {Object} [options] configuration options to pass to the viewer
     * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
     * @param {string|number} [options.width] the width of the viewer
     * @param {string|number} [options.height] the height of the viewer
     * @param {Object} [options.moddleExtensions] extension packages to provide
     * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
     * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
     */
    export default class BaseModeler extends BaseViewer {
        /**
         * A base modeler for BPMN 2.0 diagrams.
         *
         * Have a look at {@link Modeler} for a bundle that includes actual features.
         *
         * @param {Object} [options] configuration options to pass to the viewer
         * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
         * @param {string|number} [options.width] the width of the viewer
         * @param {string|number} [options.height] the height of the viewer
         * @param {Object} [options.moddleExtensions] extension packages to provide
         * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
         * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
         */
        constructor(options?: {
            container?: any;
            width?: string | number;
            height?: string | number;
            moddleExtensions?: any;
            modules?: Array<any>;
            additionalModules?: Array<any>;
        });
        _createModdle(options: any): any;
        _collectIds(definitions: any, elementsById: any): void;
    }
}
declare module "bpmn-js/lib/BaseViewer" {
    /**
     * A base viewer for BPMN 2.0 diagrams.
     *
     * Have a look at {@link Viewer}, {@link NavigatedViewer} or {@link Modeler} for
     * bundles that include actual features.
     *
     * @param {Object} [options] configuration options to pass to the viewer
     * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
     * @param {string|number} [options.width] the width of the viewer
     * @param {string|number} [options.height] the height of the viewer
     * @param {Object} [options.moddleExtensions] extension packages to provide
     * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
     * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
     */
    export default function BaseViewer(options?: {
        container: any;
        width: string | number;
        height: string | number;
        moddleExtensions: any;
        modules: Array<any>;
        additionalModules: Array<any>;
    }): void;
    export default class BaseViewer {
        /**
         * A base viewer for BPMN 2.0 diagrams.
         *
         * Have a look at {@link Viewer}, {@link NavigatedViewer} or {@link Modeler} for
         * bundles that include actual features.
         *
         * @param {Object} [options] configuration options to pass to the viewer
         * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
         * @param {string|number} [options.width] the width of the viewer
         * @param {string|number} [options.height] the height of the viewer
         * @param {Object} [options.moddleExtensions] extension packages to provide
         * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
         * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
         */
        constructor(options?: {
            container: any;
            width: string | number;
            height: string | number;
            moddleExtensions: any;
            modules: Array<any>;
            additionalModules: Array<any>;
        });
        _moddle: any;
        _container: any;
        /**
        * The importXML result.
        *
        * @typedef {Object} ImportXMLResult
        *
        * @property {Array<string>} warnings
        */
        /**
        * The importXML error.
        *
        * @typedef {Error} ImportXMLError
        *
        * @property {Array<string>} warnings
        */
        /**
         * Parse and render a BPMN 2.0 diagram.
         *
         * Once finished the viewer reports back the result to the
         * provided callback function with (err, warnings).
         *
         * ## Life-Cycle Events
         *
         * During import the viewer will fire life-cycle events:
         *
         *   * import.parse.start (about to read model from xml)
         *   * import.parse.complete (model read; may have worked or not)
         *   * import.render.start (graphical import start)
         *   * import.render.complete (graphical import finished)
         *   * import.done (everything done)
         *
         * You can use these events to hook into the life-cycle.
         *
         * @param {string} xml the BPMN 2.0 xml
         * @param {ModdleElement<BPMNDiagram>|string} [bpmnDiagram] BPMN diagram or id of diagram to render (if not provided, the first one will be rendered)
         *
         * Returns {Promise<ImportXMLResult, ImportXMLError>}
         */
        importXML: (...args: any[]) => any;
        /**
        * The importDefinitions result.
        *
        * @typedef {Object} ImportDefinitionsResult
        *
        * @property {Array<string>} warnings
        */
        /**
        * The importDefinitions error.
        *
        * @typedef {Error} ImportDefinitionsError
        *
        * @property {Array<string>} warnings
        */
        /**
         * Import parsed definitions and render a BPMN 2.0 diagram.
         *
         * Once finished the viewer reports back the result to the
         * provided callback function with (err, warnings).
         *
         * ## Life-Cycle Events
         *
         * During import the viewer will fire life-cycle events:
         *
         *   * import.render.start (graphical import start)
         *   * import.render.complete (graphical import finished)
         *
         * You can use these events to hook into the life-cycle.
         *
         * @param {ModdleElement<Definitions>} definitions parsed BPMN 2.0 definitions
         * @param {ModdleElement<BPMNDiagram>|string} [bpmnDiagram] BPMN diagram or id of diagram to render (if not provided, the first one will be rendered)
         *
         * Returns {Promise<ImportDefinitionsResult, ImportDefinitionsError>}
         */
        importDefinitions: (...args: any[]) => any;
        /**
         * The open result.
         *
         * @typedef {Object} OpenResult
         *
         * @property {Array<string>} warnings
         */
        /**
        * The open error.
        *
        * @typedef {Error} OpenError
        *
        * @property {Array<string>} warnings
        */
        /**
         * Open diagram of previously imported XML.
         *
         * Once finished the viewer reports back the result to the
         * provided callback function with (err, warnings).
         *
         * ## Life-Cycle Events
         *
         * During switch the viewer will fire life-cycle events:
         *
         *   * import.render.start (graphical import start)
         *   * import.render.complete (graphical import finished)
         *
         * You can use these events to hook into the life-cycle.
         *
         * @param {string|ModdleElement<BPMNDiagram>} [bpmnDiagramOrId] id or the diagram to open
         *
         * Returns {Promise<OpenResult, OpenError>}
         */
        open: (...args: any[]) => any;
        /**
         * The saveXML result.
         *
         * @typedef {Object} SaveXMLResult
         *
         * @property {string} xml
         */
        /**
         * Export the currently displayed BPMN 2.0 diagram as
         * a BPMN 2.0 XML document.
         *
         * ## Life-Cycle Events
         *
         * During XML saving the viewer will fire life-cycle events:
         *
         *   * saveXML.start (before serialization)
         *   * saveXML.serialized (after xml generation)
         *   * saveXML.done (everything done)
         *
         * You can use these events to hook into the life-cycle.
         *
         * @param {Object} [options] export options
         * @param {boolean} [options.format=false] output formatted XML
         * @param {boolean} [options.preamble=true] output preamble
         *
         * Returns {Promise<SaveXMLResult, Error>}
         */
        saveXML: (...args: any[]) => Promise<SaveXMLResult>;
        /**
         * The saveSVG result.
         *
         * @typedef {Object} SaveSVGResult
         *
         * @property {string} svg
         */
        /**
         * Export the currently displayed BPMN 2.0 diagram as
         * an SVG image.
         *
         * ## Life-Cycle Events
         *
         * During SVG saving the viewer will fire life-cycle events:
         *
         *   * saveSVG.start (before serialization)
         *   * saveSVG.done (everything done)
         *
         * You can use these events to hook into the life-cycle.
         *
         * @param {Object} [options]
         *
         * Returns {Promise<SaveSVGResult, Error>}
         */
        saveSVG: (...args: any[]) => Promise<SaveSVGResult>;
        _setDefinitions(definitions: any): any;
        _definitions: any;
        getModules(): any[];
        clear(): void;
        destroy(): void;
        on(event: string, priority?: number, callback?: Function, target?: any): any;
        off(event: string, callback: Function): void;
        attachTo(parentNode: any): void;
        getDefinitions(): any;
        detach(): void;
        _init(container: any, moddle: any, options: any): void;
        _emit(type: string, event: any): any;
        _createContainer(options: any): HTMLElement;
        _createModdle(options: any): any;
        _modules: any[];
    }
    /**
     * The importXML result.
     */
    export type ImportXMLResult = {
        warnings: Array<string>;
    };
    /**
     * The importXML error.
     */
    export type ImportXMLError = Error;
    /**
     * The importDefinitions result.
     */
    export type ImportDefinitionsResult = {
        warnings: Array<string>;
    };
    /**
     * The importDefinitions error.
     */
    export type ImportDefinitionsError = Error;
    /**
     * The open result.
     */
    export type OpenResult = {
        warnings: Array<string>;
    };
    /**
     * The open error.
     */
    export type OpenError = Error;
    /**
     * The saveXML result.
     */
    export type SaveXMLResult = {
        xml: string;
    };
    /**
     * The saveSVG result.
     */
    export type SaveSVGResult = {
        svg: string;
    };
}
declare module "bpmn-js/lib/Modeler" {
    import BaseModeler from 'bpmn-js/lib/BaseModeler';
    /**
     * A modeler for BPMN 2.0 diagrams.
     *
     *
     * ## Extending the Modeler
     *
     * In order to extend the viewer pass extension modules to bootstrap via the
     * `additionalModules` option. An extension module is an object that exposes
     * named services.
     *
     * The following example depicts the integration of a simple
     * logging component that integrates with interaction events:
     *
     *
     * ```javascript
     *
     * // logging component
     * function InteractionLogger(eventBus) {
     *   eventBus.on('element.hover', function(event) {
     *     console.log()
     *   })
     * }
     *
     * InteractionLogger.$inject = [ 'eventBus' ]; // minification save
     *
     * // extension module
     * var extensionModule = {
     *   __init__: [ 'interactionLogger' ],
     *   interactionLogger: [ 'type', InteractionLogger ]
     * };
     *
     * // extend the viewer
     * var bpmnModeler = new Modeler({ additionalModules: [ extensionModule ] });
     * bpmnModeler.importXML(...);
     * ```
     *
     *
     * ## Customizing / Replacing Components
     *
     * You can replace individual diagram components by redefining them in override modules.
     * This works for all components, including those defined in the core.
     *
     * Pass in override modules via the `options.additionalModules` flag like this:
     *
     * ```javascript
     * function CustomContextPadProvider(contextPad) {
     *
     *   contextPad.registerProvider(this);
     *
     *   this.getContextPadEntries = function(element) {
     *     // no entries, effectively disable the context pad
     *     return {};
     *   };
     * }
     *
     * CustomContextPadProvider.$inject = [ 'contextPad' ];
     *
     * var overrideModule = {
     *   contextPadProvider: [ 'type', CustomContextPadProvider ]
     * };
     *
     * var bpmnModeler = new Modeler({ additionalModules: [ overrideModule ]});
     * ```
     *
     * @param {Object} [options] configuration options to pass to the viewer
     * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
     * @param {string|number} [options.width] the width of the viewer
     * @param {string|number} [options.height] the height of the viewer
     * @param {Object} [options.moddleExtensions] extension packages to provide
     * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
     * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
     */
    class Modeler extends BaseModeler {
        /**
         * A modeler for BPMN 2.0 diagrams.
         *
         *
         * ## Extending the Modeler
         *
         * In order to extend the viewer pass extension modules to bootstrap via the
         * `additionalModules` option. An extension module is an object that exposes
         * named services.
         *
         * The following example depicts the integration of a simple
         * logging component that integrates with interaction events:
         *
         *
         * ```javascript
         *
         * // logging component
         * function InteractionLogger(eventBus) {
         *   eventBus.on('element.hover', function(event) {
         *     console.log()
         *   })
         * }
         *
         * InteractionLogger.$inject = [ 'eventBus' ]; // minification save
         *
         * // extension module
         * var extensionModule = {
         *   __init__: [ 'interactionLogger' ],
         *   interactionLogger: [ 'type', InteractionLogger ]
         * };
         *
         * // extend the viewer
         * var bpmnModeler = new Modeler({ additionalModules: [ extensionModule ] });
         * bpmnModeler.importXML(...);
         * ```
         *
         *
         * ## Customizing / Replacing Components
         *
         * You can replace individual diagram components by redefining them in override modules.
         * This works for all components, including those defined in the core.
         *
         * Pass in override modules via the `options.additionalModules` flag like this:
         *
         * ```javascript
         * function CustomContextPadProvider(contextPad) {
         *
         *   contextPad.registerProvider(this);
         *
         *   this.getContextPadEntries = function(element) {
         *     // no entries, effectively disable the context pad
         *     return {};
         *   };
         * }
         *
         * CustomContextPadProvider.$inject = [ 'contextPad' ];
         *
         * var overrideModule = {
         *   contextPadProvider: [ 'type', CustomContextPadProvider ]
         * };
         *
         * var bpmnModeler = new Modeler({ additionalModules: [ overrideModule ]});
         * ```
         *
         * @param {Object} [options] configuration options to pass to the viewer
         * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
         * @param {string|number} [options.width] the width of the viewer
         * @param {string|number} [options.height] the height of the viewer
         * @param {Object} [options.moddleExtensions] extension packages to provide
         * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
         * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
         */
        constructor(options?: {
            container?: any;
            width?: string | number;
            height?: string | number;
            moddleExtensions?: any;
            modules?: Array<any>;
            additionalModules?: Array<any>;
        });
        /**
        * The createDiagram result.
        *
        * @typedef {Object} CreateDiagramResult
        *
        * @property {Array<string>} warnings
        */
        /**
        * The createDiagram error.
        *
        * @typedef {Error} CreateDiagramError
        *
        * @property {Array<string>} warnings
        */
        /**
         * Create a new diagram to start modeling.
         *
         * Returns {Promise<CreateDiagramResult, CreateDiagramError>}
         */
        createDiagram: (...args: any[]) => any;
        _interactionModules: any[];
        _modelingModules: any[];
        _modules: any[];
    }
    namespace Modeler {
        export { Viewer };
        export { NavigatedViewer };
    }
    export default Modeler;
    /**
     * The createDiagram result.
     */
    export type CreateDiagramResult = {
        warnings: Array<string>;
    };
    /**
     * The createDiagram error.
     */
    export type CreateDiagramError = Error;
    import Viewer from "bpmn-js/lib/Viewer";
    import NavigatedViewer from "bpmn-js/lib/NavigatedViewer";
}
declare module "bpmn-js/lib/NavigatedViewer" {
    /**
     * A viewer that includes mouse navigation facilities
     *
     * @param {Object} options
     */
    export default function NavigatedViewer(options: any): void;
    export default class NavigatedViewer {
        /**
         * A viewer that includes mouse navigation facilities
         *
         * @param {Object} options
         */
        constructor(options: any);
        _navigationModules: any[];
        _modules: any[];
    }
}
declare module "bpmn-js/lib/Viewer" {
    /**
     * A viewer for BPMN 2.0 diagrams.
     *
     * Have a look at {@link NavigatedViewer} or {@link Modeler} for bundles that include
     * additional features.
     *
     *
     * ## Extending the Viewer
     *
     * In order to extend the viewer pass extension modules to bootstrap via the
     * `additionalModules` option. An extension module is an object that exposes
     * named services.
     *
     * The following example depicts the integration of a simple
     * logging component that integrates with interaction events:
     *
     *
     * ```javascript
     *
     * // logging component
     * function InteractionLogger(eventBus) {
     *   eventBus.on('element.hover', function(event) {
     *     console.log()
     *   })
     * }
     *
     * InteractionLogger.$inject = [ 'eventBus' ]; // minification save
     *
     * // extension module
     * var extensionModule = {
     *   __init__: [ 'interactionLogger' ],
     *   interactionLogger: [ 'type', InteractionLogger ]
     * };
     *
     * // extend the viewer
     * var bpmnViewer = new Viewer({ additionalModules: [ extensionModule ] });
     * bpmnViewer.importXML(...);
     * ```
     *
     * @param {Object} [options] configuration options to pass to the viewer
     * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
     * @param {string|number} [options.width] the width of the viewer
     * @param {string|number} [options.height] the height of the viewer
     * @param {Object} [options.moddleExtensions] extension packages to provide
     * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
     * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
     */
    export default function Viewer(options?: {
        container: any;
        width: string | number;
        height: string | number;
        moddleExtensions: any;
        modules: Array<any>;
        additionalModules: Array<any>;
    }): void;
    export default class Viewer {
        /**
         * A viewer for BPMN 2.0 diagrams.
         *
         * Have a look at {@link NavigatedViewer} or {@link Modeler} for bundles that include
         * additional features.
         *
         *
         * ## Extending the Viewer
         *
         * In order to extend the viewer pass extension modules to bootstrap via the
         * `additionalModules` option. An extension module is an object that exposes
         * named services.
         *
         * The following example depicts the integration of a simple
         * logging component that integrates with interaction events:
         *
         *
         * ```javascript
         *
         * // logging component
         * function InteractionLogger(eventBus) {
         *   eventBus.on('element.hover', function(event) {
         *     console.log()
         *   })
         * }
         *
         * InteractionLogger.$inject = [ 'eventBus' ]; // minification save
         *
         * // extension module
         * var extensionModule = {
         *   __init__: [ 'interactionLogger' ],
         *   interactionLogger: [ 'type', InteractionLogger ]
         * };
         *
         * // extend the viewer
         * var bpmnViewer = new Viewer({ additionalModules: [ extensionModule ] });
         * bpmnViewer.importXML(...);
         * ```
         *
         * @param {Object} [options] configuration options to pass to the viewer
         * @param {DOMElement} [options.container] the container to render the viewer in, defaults to body.
         * @param {string|number} [options.width] the width of the viewer
         * @param {string|number} [options.height] the height of the viewer
         * @param {Object} [options.moddleExtensions] extension packages to provide
         * @param {Array<didi.Module>} [options.modules] a list of modules to override the default modules
         * @param {Array<didi.Module>} [options.additionalModules] a list of modules to use with the default modules
         */
        constructor(options?: {
            container: any;
            width: string | number;
            height: string | number;
            moddleExtensions: any;
            modules: Array<any>;
            additionalModules: Array<any>;
        });
        _modules: any[];
        _moddleExtensions: {};
    }
}
declare module "bpmn-js/lib/core/index" {
    namespace _default {
        export const __depends__: ({
            __init__: string[];
            bpmnRenderer: (string | typeof import("bpmn-js/lib/draw/BpmnRenderer").default)[];
            textRenderer: (string | typeof import("bpmn-js/lib/draw/TextRenderer").default)[];
            pathMap: (string | typeof import("bpmn-js/lib/draw/PathMap").default)[];
        } | {
            __depends__: any[];
            bpmnImporter: (string | typeof import("bpmn-js/lib/import/BpmnImporter").default)[];
        })[];
    }
    export default _default;
}
declare module "bpmn-js/lib/draw/BpmnRenderUtil" {
    /**
     * Checks if eventDefinition of the given element matches with semantic type.
     *
     * @return {boolean} true if element is of the given semantic type
     */
    export function isTypedEvent(event: any, eventDefinitionType: any, filter: any): boolean;
    export function isThrowEvent(event: any): boolean;
    export function isCollection(element: any): any;
    export function getDi(element: any): any;
    export function getSemantic(element: any): any;
    export function getFillColor(element: any, defaultColor: any): any;
    export function getStrokeColor(element: any, defaultColor: any): any;
    export function getCirclePath(shape: any): any;
    export function getRoundRectPath(shape: any, borderRadius: any): any;
    export function getDiamondPath(shape: any): any;
    export function getRectPath(shape: any): any;
}
declare module "bpmn-js/lib/draw/BpmnRenderer" {
    function BpmnRenderer(config: any, eventBus: any, styles: any, pathMap: any, canvas: any, textRenderer: any, priority: any): void;
    class BpmnRenderer {
        constructor(config: any, eventBus: any, styles: any, pathMap: any, canvas: any, textRenderer: any, priority: any);
        handlers: {
            'bpmn:Event': (parentGfx: any, element: any, attrs: any) => SVGCircleElement;
            'bpmn:StartEvent': (parentGfx: any, element: any) => any;
            'bpmn:MessageEventDefinition': (parentGfx: any, element: any, isThrowing: any) => SVGPathElement;
            'bpmn:TimerEventDefinition': (parentGfx: any, element: any) => SVGCircleElement;
            'bpmn:EscalationEventDefinition': (parentGfx: any, event: any, isThrowing: any) => SVGPathElement;
            'bpmn:ConditionalEventDefinition': (parentGfx: any, event: any) => SVGPathElement;
            'bpmn:LinkEventDefinition': (parentGfx: any, event: any, isThrowing: any) => SVGPathElement;
            'bpmn:ErrorEventDefinition': (parentGfx: any, event: any, isThrowing: any) => SVGPathElement;
            'bpmn:CancelEventDefinition': (parentGfx: any, event: any, isThrowing: any) => SVGPathElement;
            'bpmn:CompensateEventDefinition': (parentGfx: any, event: any, isThrowing: any) => SVGPathElement;
            'bpmn:SignalEventDefinition': (parentGfx: any, event: any, isThrowing: any) => SVGPathElement;
            'bpmn:MultipleEventDefinition': (parentGfx: any, event: any, isThrowing: any) => SVGPathElement;
            'bpmn:ParallelMultipleEventDefinition': (parentGfx: any, event: any) => SVGPathElement;
            'bpmn:EndEvent': (parentGfx: any, element: any) => any;
            'bpmn:TerminateEventDefinition': (parentGfx: any, element: any) => SVGCircleElement;
            'bpmn:IntermediateEvent': (parentGfx: any, element: any) => any;
            'bpmn:IntermediateCatchEvent': (parentGfx: any, element: any) => any;
            'bpmn:IntermediateThrowEvent': (parentGfx: any, element: any) => any;
            'bpmn:Activity': (parentGfx: any, element: any, attrs: any) => SVGRectElement;
            'bpmn:Task': (parentGfx: any, element: any) => any;
            'bpmn:ServiceTask': (parentGfx: any, element: any) => any;
            'bpmn:UserTask': (parentGfx: any, element: any) => any;
            'bpmn:ManualTask': (parentGfx: any, element: any) => any;
            'bpmn:SendTask': (parentGfx: any, element: any) => any;
            'bpmn:ReceiveTask': (parentGfx: any, element: any) => any;
            'bpmn:ScriptTask': (parentGfx: any, element: any) => any;
            'bpmn:BusinessRuleTask': (parentGfx: any, element: any) => any;
            'bpmn:SubProcess': (parentGfx: any, element: any, attrs: any) => any;
            'bpmn:AdHocSubProcess': (parentGfx: any, element: any) => any;
            'bpmn:Transaction': (parentGfx: any, element: any) => any;
            'bpmn:CallActivity': (parentGfx: any, element: any) => any;
            'bpmn:Participant': (parentGfx: any, element: any) => any;
            'bpmn:Lane': (parentGfx: any, element: any, attrs: any) => SVGRectElement;
            'bpmn:InclusiveGateway': (parentGfx: any, element: any) => any;
            'bpmn:ExclusiveGateway': (parentGfx: any, element: any) => any;
            'bpmn:ComplexGateway': (parentGfx: any, element: any) => any;
            'bpmn:ParallelGateway': (parentGfx: any, element: any) => any;
            'bpmn:EventBasedGateway': (parentGfx: any, element: any) => any;
            'bpmn:Gateway': (parentGfx: any, element: any) => SVGPolygonElement;
            'bpmn:SequenceFlow': (parentGfx: any, element: any) => SVGPathElement;
            'bpmn:Association': (parentGfx: any, element: any, attrs: any) => any;
            'bpmn:DataInputAssociation': (parentGfx: any, element: any) => any;
            'bpmn:DataOutputAssociation': (parentGfx: any, element: any) => any;
            'bpmn:MessageFlow': (parentGfx: any, element: any) => SVGPathElement;
            'bpmn:DataObject': (parentGfx: any, element: any) => SVGPathElement;
            'bpmn:DataObjectReference': (parentGfx: any, element: any) => any;
            'bpmn:DataInput': (parentGfx: any, element: any) => any;
            'bpmn:DataOutput': (parentGfx: any, element: any) => any;
            'bpmn:DataStoreReference': (parentGfx: any, element: any) => SVGPathElement;
            'bpmn:BoundaryEvent': (parentGfx: any, element: any) => any;
            'bpmn:Group': (parentGfx: any, element: any) => SVGRectElement;
            label: (parentGfx: any, element: any) => any;
            'bpmn:TextAnnotation': (parentGfx: any, element: any) => SVGRectElement;
            ParticipantMultiplicityMarker: (parentGfx: any, element: any) => void;
            SubProcessMarker: (parentGfx: any, element: any) => void;
            ParallelMarker: (parentGfx: any, element: any, position: any) => void;
            SequentialMarker: (parentGfx: any, element: any, position: any) => void;
            CompensationMarker: (parentGfx: any, element: any, position: any) => void;
            LoopMarker: (parentGfx: any, element: any, position: any) => void;
            AdhocMarker: (parentGfx: any, element: any, position: any) => void;
        };
        _drawPath: (parentGfx: any, d: any, attrs: any) => SVGPathElement;
        canRender(element: any): boolean;
        drawShape(parentGfx: any, element: any): any;
        drawConnection(parentGfx: any, element: any): any;
        getShapePath(element: any): any;
    }
    namespace BpmnRenderer {
        export const $inject: string[];
    }
    export default BpmnRenderer;
}
declare module "bpmn-js/lib/draw/PathMap" {
    /**
     * Map containing SVG paths needed by BpmnRenderer.
     */
    export default function PathMap(): void;
    export default class PathMap {
        /**
         * Contains a map of path elements
         *
         * <h1>Path definition</h1>
         * A parameterized path is defined like this:
         * <pre>
         * 'GATEWAY_PARALLEL': {
         *   d: 'm {mx},{my} {e.x0},0 0,{e.x1} {e.x1},0 0,{e.y0} -{e.x1},0 0,{e.y1} ' +
                '-{e.x0},0 0,-{e.y1} -{e.x1},0 0,-{e.y0} {e.x1},0 z',
         *   height: 17.5,
         *   width:  17.5,
         *   heightElements: [2.5, 7.5],
         *   widthElements: [2.5, 7.5]
         * }
         * </pre>
         * <p>It's important to specify a correct <b>height and width</b> for the path as the scaling
         * is based on the ratio between the specified height and width in this object and the
         * height and width that is set as scale target (Note x,y coordinates will be scaled with
         * individual ratios).</p>
         * <p>The '<b>heightElements</b>' and '<b>widthElements</b>' array must contain the values that will be scaled.
         * The scaling is based on the computed ratios.
         * Coordinates on the y axis should be in the <b>heightElement</b>'s array, they will be scaled using
         * the computed ratio coefficient.
         * In the parameterized path the scaled values can be accessed through the 'e' object in {} brackets.
         *   <ul>
         *    <li>The values for the y axis can be accessed in the path string using {e.y0}, {e.y1}, ....</li>
         *    <li>The values for the x axis can be accessed in the path string using {e.x0}, {e.x1}, ....</li>
         *   </ul>
         *   The numbers x0, x1 respectively y0, y1, ... map to the corresponding array index.
         * </p>
         */
        pathMap: {
            EVENT_MESSAGE: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_SIGNAL: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_ESCALATION: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_CONDITIONAL: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_LINK: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_ERROR: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_CANCEL_45: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_COMPENSATION: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_TIMER_WH: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_TIMER_LINE: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_MULTIPLE: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            EVENT_PARALLEL_MULTIPLE: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            GATEWAY_EXCLUSIVE: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            GATEWAY_PARALLEL: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            GATEWAY_EVENT_BASED: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            GATEWAY_COMPLEX: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            DATA_OBJECT_PATH: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            DATA_OBJECT_COLLECTION_PATH: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            DATA_ARROW: {
                d: string;
                height: number;
                width: number;
                heightElements: any[];
                widthElements: any[];
            };
            DATA_STORE: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            TEXT_ANNOTATION: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            MARKER_SUB_PROCESS: {
                d: string;
                height: number;
                width: number;
                heightElements: any[];
                widthElements: any[];
            };
            MARKER_PARALLEL: {
                d: string;
                height: number;
                width: number;
                heightElements: any[];
                widthElements: any[];
            };
            MARKER_SEQUENTIAL: {
                d: string;
                height: number;
                width: number;
                heightElements: any[];
                widthElements: any[];
            };
            MARKER_COMPENSATION: {
                d: string;
                height: number;
                width: number;
                heightElements: any[];
                widthElements: any[];
            };
            MARKER_LOOP: {
                d: string;
                height: number;
                width: number;
                heightElements: any[];
                widthElements: any[];
            };
            MARKER_ADHOC: {
                d: string;
                height: number;
                width: number;
                heightElements: any[];
                widthElements: any[];
            };
            TASK_TYPE_SEND: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            TASK_TYPE_SCRIPT: {
                d: string;
                height: number;
                width: number;
                heightElements: number[];
                widthElements: number[];
            };
            TASK_TYPE_USER_1: {
                d: string;
            };
            TASK_TYPE_USER_2: {
                d: string;
            };
            TASK_TYPE_USER_3: {
                d: string;
            };
            TASK_TYPE_MANUAL: {
                d: string;
            };
            TASK_TYPE_INSTANTIATING_SEND: {
                d: string;
            };
            TASK_TYPE_SERVICE: {
                d: string;
            };
            TASK_TYPE_SERVICE_FILL: {
                d: string;
            };
            TASK_TYPE_BUSINESS_RULE_HEADER: {
                d: string;
            };
            TASK_TYPE_BUSINESS_RULE_MAIN: {
                d: string;
            };
            MESSAGE_FLOW_MARKER: {
                d: string;
            };
        };
        getRawPath: (pathId: any) => any;
        /**
         * Scales the path to the given height and width.
         * <h1>Use case</h1>
         * <p>Use case is to scale the content of elements (event, gateways) based
         * on the element bounding box's size.
         * </p>
         * <h1>Why not transform</h1>
         * <p>Scaling a path with transform() will also scale the stroke and IE does not support
         * the option 'non-scaling-stroke' to prevent this.
         * Also there are use cases where only some parts of a path should be
         * scaled.</p>
         *
         * @param {string} pathId The ID of the path.
         * @param {Object} param <p>
         *   Example param object scales the path to 60% size of the container (data.width, data.height).
         *   <pre>
         *   {
         *     xScaleFactor: 0.6,
         *     yScaleFactor:0.6,
         *     containerWidth: data.width,
         *     containerHeight: data.height,
         *     position: {
         *       mx: 0.46,
         *       my: 0.2,
         *     }
         *   }
         *   </pre>
         *   <ul>
         *    <li>targetpathwidth = xScaleFactor * containerWidth</li>
         *    <li>targetpathheight = yScaleFactor * containerHeight</li>
         *    <li>Position is used to set the starting coordinate of the path. M is computed:
          *    <ul>
          *      <li>position.x * containerWidth</li>
          *      <li>position.y * containerHeight</li>
          *    </ul>
          *    Center of the container <pre> position: {
         *       mx: 0.5,
         *       my: 0.5,
         *     }</pre>
         *     Upper left corner of the container
         *     <pre> position: {
         *       mx: 0.0,
         *       my: 0.0,
         *     }</pre>
         *    </li>
         *   </ul>
         * </p>
         *
         */
        getScaledPath: (pathId: string, param: any) => string;
    }
}
declare module "bpmn-js/lib/draw/TextRenderer" {
    function TextRenderer(config: any): void;
    class TextRenderer {
        constructor(config: any);
        /**
         * Get the new bounds of an externally rendered,
         * layouted label.
         *
         * @param  {Bounds} bounds
         * @param  {string} text
         *
         * @return {Bounds}
         */
        getExternalLabelBounds: (bounds: any, text: string) => any;
        /**
         * Get the new bounds of text annotation.
         *
         * @param  {Bounds} bounds
         * @param  {string} text
         *
         * @return {Bounds}
         */
        getTextAnnotationBounds: (bounds: any, text: string) => any;
        /**
         * Create a layouted text element.
         *
         * @param {string} text
         * @param {Object} [options]
         *
         * @return {SVGElement} rendered text
         */
        createText: (text: string, options?: any) => SVGElement;
        /**
         * Get default text style.
         */
        getDefaultStyle: () => any;
        /**
         * Get the external text style.
         */
        getExternalStyle: () => any;
    }
    namespace TextRenderer {
        export const $inject: string[];
    }
    export default TextRenderer;
}
declare module "bpmn-js/lib/draw/index" {
    namespace _default {
        export const __init__: string[];
        export const bpmnRenderer: (string | typeof BpmnRenderer)[];
        export const textRenderer: (string | typeof TextRenderer)[];
        export const pathMap: (string | typeof PathMap)[];
    }
    export default _default;
    import BpmnRenderer from "bpmn-js/lib/draw/BpmnRenderer";
    import TextRenderer from "bpmn-js/lib/draw/TextRenderer";
    import PathMap from "bpmn-js/lib/draw/PathMap";
}
declare module "bpmn-js/lib/features/auto-place/BpmnAutoPlace" {
    /**
     * BPMN auto-place behavior.
     *
     * @param {EventBus} eventBus
     */
    function AutoPlace(eventBus: any): void;
    namespace AutoPlace {
        export const $inject: string[];
    }
    export default AutoPlace;
}
declare module "bpmn-js/lib/features/auto-place/BpmnAutoPlaceUtil" {
    /**
     * Find the new position for the target element to
     * connect to source.
     *
     * @param  {djs.model.Shape} source
     * @param  {djs.model.Shape} element
     *
     * @return {Point}
     */
    export function getNewShapePosition(source: any, element: any): any;
    /**
     * Always try to place element right of source;
     * compute actual distance from previous nodes in flow.
     */
    export function getFlowNodePosition(source: any, element: any): any;
    /**
     * Always try to place text annotations top right of source.
     */
    export function getTextAnnotationPosition(source: any, element: any): any;
    /**
     * Always put element bottom right of source.
     */
    export function getDataElementPosition(source: any, element: any): any;
}
declare module "bpmn-js/lib/features/auto-place/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const bpmnAutoPlace: (string | typeof BpmnAutoPlace)[];
    }
    export default _default;
    import BpmnAutoPlace from "bpmn-js/lib/features/auto-place/BpmnAutoPlace";
}
declare module "bpmn-js/lib/features/auto-resize/BpmnAutoResize" {
    /**
     * Sub class of the AutoResize module which implements a BPMN
     * specific resize function.
     */
    function BpmnAutoResize(injector: any): void;
    class BpmnAutoResize {
        /**
         * Sub class of the AutoResize module which implements a BPMN
         * specific resize function.
         */
        constructor(injector: any);
        resize(target: any, newBounds: any, hints: any): void;
    }
    namespace BpmnAutoResize {
        export const $inject: string[];
    }
    export default BpmnAutoResize;
}
declare module "bpmn-js/lib/features/auto-resize/BpmnAutoResizeProvider" {
    /**
     * This module is a provider for automatically resizing parent BPMN elements
     */
    function BpmnAutoResizeProvider(eventBus: any, modeling: any): void;
    class BpmnAutoResizeProvider {
        /**
         * This module is a provider for automatically resizing parent BPMN elements
         */
        constructor(eventBus: any, modeling: any);
        _modeling: any;
        canResize(elements: any, target: any): boolean;
    }
    namespace BpmnAutoResizeProvider {
        export const $inject: string[];
    }
    export default BpmnAutoResizeProvider;
}
declare module "bpmn-js/lib/features/auto-resize/index" {
    namespace _default {
        export const __init__: string[];
        export const bpmnAutoResize: (string | typeof BpmnAutoResize)[];
        export const bpmnAutoResizeProvider: (string | typeof BpmnAutoResizeProvider)[];
    }
    export default _default;
    import BpmnAutoResize from "bpmn-js/lib/features/auto-resize/BpmnAutoResize";
    import BpmnAutoResizeProvider from "bpmn-js/lib/features/auto-resize/BpmnAutoResizeProvider";
}
declare module "bpmn-js/lib/features/context-pad/ContextPadProvider" {
    /**
     * A provider for BPMN 2.0 elements context pad
     */
    function ContextPadProvider(config: any, injector: any, eventBus: any, contextPad: any, modeling: any, elementFactory: any, connect: any, create: any, popupMenu: any, canvas: any, rules: any, translate: any): void;
    class ContextPadProvider {
        /**
         * A provider for BPMN 2.0 elements context pad
         */
        constructor(config: any, injector: any, eventBus: any, contextPad: any, modeling: any, elementFactory: any, connect: any, create: any, popupMenu: any, canvas: any, rules: any, translate: any);
        _contextPad: any;
        _modeling: any;
        _elementFactory: any;
        _connect: any;
        _create: any;
        _popupMenu: any;
        _canvas: any;
        _rules: any;
        _translate: any;
        _autoPlace: any;
        getContextPadEntries(element: any): {};
    }
    namespace ContextPadProvider {
        export const $inject: string[];
    }
    export default ContextPadProvider;
}
declare module "bpmn-js/lib/features/context-pad/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const contextPadProvider: (string | typeof ContextPadProvider)[];
    }
    export default _default;
    import ContextPadProvider from "bpmn-js/lib/features/context-pad/ContextPadProvider";
}
declare module "bpmn-js/lib/features/copy-paste/BpmnCopyPaste" {
    function BpmnCopyPaste(bpmnFactory: any, eventBus: any, moddleCopy: any): void;
    namespace BpmnCopyPaste {
        export const $inject: string[];
    }
    export default BpmnCopyPaste;
}
declare module "bpmn-js/lib/features/copy-paste/ModdleCopy" {
    /**
     * @typedef {Function} <moddleCopy.canCopyProperties> listener
     *
     * @param {Object} context
     * @param {Array<string>} context.propertyNames
     * @param {ModdleElement} context.sourceElement
     * @param {ModdleElement} context.targetElement
     *
     * @returns {Array<string>|boolean} - Return properties to be copied or false to disallow
     * copying.
     */
    /**
     * @typedef {Function} <moddleCopy.canCopyProperty> listener
     *
     * @param {Object} context
     * @param {ModdleElement} context.parent
     * @param {*} context.property
     * @param {string} context.propertyName
     *
     * @returns {*|boolean} - Return copied property or false to disallow
     * copying.
     */
    /**
     * @typedef {Function} <moddleCopy.canSetCopiedProperty> listener
     *
     * @param {Object} context
     * @param {ModdleElement} context.parent
     * @param {*} context.property
     * @param {string} context.propertyName
     *
     * @returns {boolean} - Return false to disallow
     * setting copied property.
     */
    /**
     * Utility for copying model properties from source element to target element.
     *
     * @param {EventBus} eventBus
     * @param {BpmnFactory} bpmnFactory
     * @param {BpmnModdle} moddle
     */
    function ModdleCopy(eventBus: any, bpmnFactory: any, moddle: any): void;
    class ModdleCopy {
        /**
         * @typedef {Function} <moddleCopy.canCopyProperties> listener
         *
         * @param {Object} context
         * @param {Array<string>} context.propertyNames
         * @param {ModdleElement} context.sourceElement
         * @param {ModdleElement} context.targetElement
         *
         * @returns {Array<string>|boolean} - Return properties to be copied or false to disallow
         * copying.
         */
        /**
         * @typedef {Function} <moddleCopy.canCopyProperty> listener
         *
         * @param {Object} context
         * @param {ModdleElement} context.parent
         * @param {*} context.property
         * @param {string} context.propertyName
         *
         * @returns {*|boolean} - Return copied property or false to disallow
         * copying.
         */
        /**
         * @typedef {Function} <moddleCopy.canSetCopiedProperty> listener
         *
         * @param {Object} context
         * @param {ModdleElement} context.parent
         * @param {*} context.property
         * @param {string} context.propertyName
         *
         * @returns {boolean} - Return false to disallow
         * setting copied property.
         */
        /**
         * Utility for copying model properties from source element to target element.
         *
         * @param {EventBus} eventBus
         * @param {BpmnFactory} bpmnFactory
         * @param {BpmnModdle} moddle
         */
        constructor(eventBus: any, bpmnFactory: any, moddle: any);
        _bpmnFactory: any;
        _eventBus: any;
        _moddle: any;
        copyElement(sourceElement: any, targetElement: any, propertyNames?: Array<string>): any;
        copyProperty(property: any, parent: any, propertyName: string): any;
    }
    namespace ModdleCopy {
        export const $inject: string[];
    }
    export default ModdleCopy;
    export function getPropertyNames(descriptor: any, keepDefaultProperties: any): any;
    /**
     * <moddleCopy.canCopyProperties> listener
     */
    //export type ModdleCopy = Function;
}
declare module "bpmn-js/lib/features/copy-paste/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const bpmnCopyPaste: (string | typeof BpmnCopyPaste)[];
        export const moddleCopy: (string | typeof ModdleCopy)[];
    }
    export default _default;
    import BpmnCopyPaste from "bpmn-js/lib/features/copy-paste/BpmnCopyPaste";
    import ModdleCopy from "bpmn-js/lib/features/copy-paste/ModdleCopy";
}
declare module "bpmn-js/lib/features/di-ordering/BpmnDiOrdering" {
    function BpmnDiOrdering(eventBus: any, canvas: any): void;
    namespace BpmnDiOrdering {
        export const $inject: string[];
    }
    export default BpmnDiOrdering;
}
declare module "bpmn-js/lib/features/di-ordering/index" {
    namespace _default {
        export const __init__: string[];
        export const bpmnDiOrdering: (string | typeof BpmnDiOrdering)[];
    }
    export default _default;
    import BpmnDiOrdering from "bpmn-js/lib/features/di-ordering/BpmnDiOrdering";
}
declare module "bpmn-js/lib/features/distribute-elements/BpmnDistributeElements" {
    /**
     * Registers element exclude filters for elements that
     * currently do not support distribution.
     */
    function BpmnDistributeElements(distributeElements: any): void;
    namespace BpmnDistributeElements {
        export const $inject: string[];
    }
    export default BpmnDistributeElements;
}
declare module "bpmn-js/lib/features/distribute-elements/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const bpmnDistributeElements: (string | typeof BpmnDistributeElements)[];
    }
    export default _default;
    import BpmnDistributeElements from "bpmn-js/lib/features/distribute-elements/BpmnDistributeElements";
}
declare module "bpmn-js/lib/features/editor-actions/BpmnEditorActions" {
    /**
     * Registers and executes BPMN specific editor actions.
     *
     * @param {Injector} injector
     */
    function BpmnEditorActions(injector: any): void;
    class BpmnEditorActions {
        /**
         * Registers and executes BPMN specific editor actions.
         *
         * @param {Injector} injector
         */
        constructor(injector: any);
        _registerDefaultActions(injector: any): void;
    }
    namespace BpmnEditorActions {
        export const $inject: string[];
    }
    export default BpmnEditorActions;
}
declare module "bpmn-js/lib/features/editor-actions/index" {
    namespace _default {
        export const __depends__: any[];
        export const editorActions: (string | typeof BpmnEditorActions)[];
    }
    export default _default;
    import BpmnEditorActions from "bpmn-js/lib/features/editor-actions/BpmnEditorActions";
}
declare module "bpmn-js/lib/features/grid-snapping/BpmnGridSnapping" {
    function BpmnGridSnapping(eventBus: any): void;
    namespace BpmnGridSnapping {
        export const $inject: string[];
    }
    export default BpmnGridSnapping;
}
declare module "bpmn-js/lib/features/grid-snapping/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const bpmnGridSnapping: (string | typeof BpmnGridSnapping)[];
    }
    export default _default;
    import BpmnGridSnapping from "bpmn-js/lib/features/grid-snapping/BpmnGridSnapping";
}
declare module "bpmn-js/lib/features/grid-snapping/behavior/AutoPlaceBehavior" {
    function AutoPlaceBehavior(eventBus: any, gridSnapping: any): void;
    namespace AutoPlaceBehavior {
        export const $inject: string[];
    }
    export default AutoPlaceBehavior;
}
declare module "bpmn-js/lib/features/grid-snapping/behavior/CreateParticipantBehavior" {
    function CreateParticipantBehavior(canvas: any, eventBus: any, gridSnapping: any): void;
    namespace CreateParticipantBehavior {
        export const $inject: string[];
    }
    export default CreateParticipantBehavior;
}
declare module "bpmn-js/lib/features/grid-snapping/behavior/LayoutConnectionBehavior" {
    /**
     * Snaps connections with Manhattan layout.
     */
    function LayoutConnectionBehavior(eventBus: any, gridSnapping: any, modeling: any): void;
    class LayoutConnectionBehavior {
        /**
         * Snaps connections with Manhattan layout.
         */
        constructor(eventBus: any, gridSnapping: any, modeling: any);
        _gridSnapping: any;
        snapMiddleSegments(waypoints: Array<any>): Array<any>;
    }
    namespace LayoutConnectionBehavior {
        export const $inject: string[];
    }
    export default LayoutConnectionBehavior;
}
declare module "bpmn-js/lib/features/grid-snapping/behavior/index" {
    namespace _default {
        export const __init__: string[];
        export const gridSnappingAutoPlaceBehavior: (string | typeof AutoPlaceBehavior)[];
        export const gridSnappingCreateParticipantBehavior: (string | typeof CreateParticipantBehavior)[];
        export const gridSnappingLayoutConnectionBehavior: (string | typeof LayoutConnectionBehavior)[];
    }
    export default _default;
    import AutoPlaceBehavior from "bpmn-js/lib/features/grid-snapping/behavior/AutoPlaceBehavior";
    import CreateParticipantBehavior from "bpmn-js/lib/features/grid-snapping/behavior/CreateParticipantBehavior";
    import LayoutConnectionBehavior from "bpmn-js/lib/features/grid-snapping/behavior/LayoutConnectionBehavior";
}
declare module "bpmn-js/lib/features/interaction-events/BpmnInteractionEvents" {
    /**
     * BPMN-specific hit zones and interaction fixes.
     *
     * @param {EventBus} eventBus
     * @param {InteractionEvents} interactionEvents
     */
    function BpmnInteractionEvents(eventBus: any, interactionEvents: any): void;
    class BpmnInteractionEvents {
        /**
         * BPMN-specific hit zones and interaction fixes.
         *
         * @param {EventBus} eventBus
         * @param {InteractionEvents} interactionEvents
         */
        constructor(eventBus: any, interactionEvents: any);
        _interactionEvents: any;
        createDefaultHit(element: any, gfx: any): boolean;
        createParticipantHit(element: any, gfx: any): boolean;
        createSubProcessHit(element: any, gfx: any): boolean;
    }
    namespace BpmnInteractionEvents {
        export const $inject: string[];
    }
    export default BpmnInteractionEvents;
}
declare module "bpmn-js/lib/features/interaction-events/index" {
    namespace _default {
        export const __init__: string[];
        export const bpmnInteractionEvents: (string | typeof BpmnInteractionEvents)[];
    }
    export default _default;
    import BpmnInteractionEvents from "bpmn-js/lib/features/interaction-events/BpmnInteractionEvents";
}
declare module "bpmn-js/lib/features/keyboard/BpmnKeyboardBindings" {
    /**
     * BPMN 2.0 specific keyboard bindings.
     *
     * @param {Injector} injector
     */
    function BpmnKeyboardBindings(injector: any): void;
    class BpmnKeyboardBindings {
        /**
         * BPMN 2.0 specific keyboard bindings.
         *
         * @param {Injector} injector
         */
        constructor(injector: any);
        registerBindings(keyboard: any, editorActions: any): void;
    }
    namespace BpmnKeyboardBindings {
        export const $inject: string[];
    }
    export default BpmnKeyboardBindings;
}
declare module "bpmn-js/lib/features/keyboard/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const keyboardBindings: (string | typeof BpmnKeyboardBindings)[];
    }
    export default _default;
    import BpmnKeyboardBindings from "bpmn-js/lib/features/keyboard/BpmnKeyboardBindings";
}
declare module "bpmn-js/lib/features/label-editing/LabelEditingPreview" {
    function LabelEditingPreview(eventBus: any, canvas: any, elementRegistry: any, pathMap: any): void;
    namespace LabelEditingPreview {
        export const $inject: string[];
    }
    export default LabelEditingPreview;
}
declare module "bpmn-js/lib/features/label-editing/LabelEditingProvider" {
    function LabelEditingProvider(eventBus: any, bpmnFactory: any, canvas: any, directEditing: any, modeling: any, resizeHandles: any, textRenderer: any): void;
    class LabelEditingProvider {
        constructor(eventBus: any, bpmnFactory: any, canvas: any, directEditing: any, modeling: any, resizeHandles: any, textRenderer: any);
        _bpmnFactory: any;
        _canvas: any;
        _modeling: any;
        _textRenderer: any;
        activate(element: any): any;
        getEditingBBox(element: any): any;
        update(element: any, newLabel: any, activeContextText: any, bounds: any): void;
    }
    namespace LabelEditingProvider {
        export const $inject: string[];
    }
    export default LabelEditingProvider;
}
declare module "bpmn-js/lib/features/label-editing/LabelUtil" {
    export function getLabel(element: any): any;
    export function setLabel(element: any, text: any, isExternal: any): any;
}
declare module "bpmn-js/lib/features/label-editing/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const labelEditingProvider: (string | typeof LabelEditingProvider)[];
        export const labelEditingPreview: (string | typeof LabelEditingPreview)[];
    }
    export default _default;
    import LabelEditingProvider from "bpmn-js/lib/features/label-editing/LabelEditingProvider";
    import LabelEditingPreview from "bpmn-js/lib/features/label-editing/LabelEditingPreview";
}
declare module "bpmn-js/lib/features/label-editing/cmd/UpdateLabelHandler" {
    /**
     * A handler that updates the text of a BPMN element.
     */
    function UpdateLabelHandler(modeling: any, textRenderer: any): void;
    class UpdateLabelHandler {
        /**
         * A handler that updates the text of a BPMN element.
         */
        constructor(modeling: any, textRenderer: any);
        preExecute: (ctx: any) => void;
        execute: (ctx: any) => any[];
        revert: (ctx: any) => any[];
        postExecute: (ctx: any) => void;
    }
    namespace UpdateLabelHandler {
        export const $inject: string[];
    }
    export default UpdateLabelHandler;
}
declare module "bpmn-js/lib/features/modeling/BpmnFactory" {
    function BpmnFactory(moddle: any): void;
    class BpmnFactory {
        constructor(moddle: any);
        _model: any;
        _needsId(element: any): boolean;
        _ensureId(element: any): void;
        create(type: any, attrs: any): any;
        createDiLabel(): any;
        createDiShape(semantic: any, bounds: any, attrs: any): any;
        createDiBounds(bounds: any): any;
        createDiWaypoints(waypoints: any): any[];
        createDiWaypoint(point: any): any;
        createDiEdge(semantic: any, waypoints: any, attrs: any): any;
        createDiPlane(semantic: any): any;
    }
    namespace BpmnFactory {
        export const $inject: string[];
    }
    export default BpmnFactory;
}
declare module "bpmn-js/lib/features/modeling/BpmnLayouter" {
    export default function BpmnLayouter(): void;
    export default class BpmnLayouter {
        layoutConnection(connection: any, hints: any): any;
    }
}
declare module "bpmn-js/lib/features/modeling/BpmnUpdater" {
    /**
     * A handler responsible for updating the underlying BPMN 2.0 XML + DI
     * once changes on the diagram happen
     */
    function BpmnUpdater(eventBus: any, bpmnFactory: any, connectionDocking: any, translate: any): void;
    class BpmnUpdater {
        /**
         * A handler responsible for updating the underlying BPMN 2.0 XML + DI
         * once changes on the diagram happen
         */
        constructor(eventBus: any, bpmnFactory: any, connectionDocking: any, translate: any);
        _bpmnFactory: any;
        _translate: any;
        updateAttachment(context: any): void;
        updateParent(element: any, oldParent: any): void;
        updateBounds(shape: any): void;
        updateFlowNodeRefs(businessObject: any, newContainment: any, oldContainment: any): void;
        updateDiConnection(di: any, newSource: any, newTarget: any): void;
        updateDiParent(di: any, parentDi: any): void;
        getLaneSet(container: any): any;
        updateSemanticParent(businessObject: any, newParent: any, visualParent: any): void;
        updateConnectionWaypoints(connection: any): void;
        updateConnection(context: any): void;
        _getLabel(di: any): any;
    }
    namespace BpmnUpdater {
        export const $inject: string[];
    }
    export default BpmnUpdater;
}
declare module "bpmn-js/lib/features/modeling/ElementFactory" {
    /**
     * A bpmn-aware factory for diagram-js shapes
     */
    function ElementFactory(bpmnFactory: any, moddle: any, translate: any): void;
    class ElementFactory {
        /**
         * A bpmn-aware factory for diagram-js shapes
         */
        constructor(bpmnFactory: any, moddle: any, translate: any);
        _bpmnFactory: any;
        _moddle: any;
        _translate: any;
        baseCreate: any;
        create(elementType: any, attrs: any): any;
        createBpmnElement(elementType: any, attrs: any): any;
        _getDefaultSize(semantic: any): {
            width: number;
            height: number;
        };
        createParticipantShape(attrs?: boolean | any): any;
    }
    namespace ElementFactory {
        export const $inject: string[];
    }
    export default ElementFactory;
}
declare module "bpmn-js/lib/features/modeling/Modeling" {
    /**
     * BPMN 2.0 modeling features activator
     *
     * @param {EventBus} eventBus
     * @param {ElementFactory} elementFactory
     * @param {CommandStack} commandStack
     * @param {BpmnRules} bpmnRules
     */
    function Modeling(eventBus: any, elementFactory: any, commandStack: any, bpmnRules: any): void;
    class Modeling {
        /**
         * BPMN 2.0 modeling features activator
         *
         * @param {EventBus} eventBus
         * @param {ElementFactory} elementFactory
         * @param {CommandStack} commandStack
         * @param {BpmnRules} bpmnRules
         */
        constructor(eventBus: any, elementFactory: any, commandStack: any, bpmnRules: any);
        _bpmnRules: any;
        getHandlers(): any;
        updateLabel(element: any, newLabel: any, newBounds: any, hints: any): void;
        connect(source: any, target: any, attrs: any, hints: any): any;
        updateProperties(element: any, properties: any): void;
        resizeLane(laneShape: any, newBounds: any, balanced: any): void;
        addLane(targetLaneShape: any, location: any): any;
        splitLane(targetLane: any, count: any): void;
        makeCollaboration(): any;
        updateLaneRefs(flowNodeShapes: any, laneShapes: any): void;
        makeProcess(): any;
        claimId(id: any, moddleElement: any): void;
        unclaimId(id: any, moddleElement: any): void;
        setColor(elements: any, colors: any): void;
    }
    namespace Modeling {
        export const $inject: string[];
    }
    export default Modeling;
}
declare module "bpmn-js/lib/features/modeling/index" {
    namespace _default {
        export const __init__: string[];
        export const __depends__: any[];
        export const bpmnFactory: (string | typeof BpmnFactory)[];
        export const bpmnUpdater: (string | typeof BpmnUpdater)[];
        export const elementFactory: (string | typeof ElementFactory)[];
        export const modeling: (string | typeof Modeling)[];
        export const layouter: (string | typeof BpmnLayouter)[];
        export const connectionDocking: any[];
    }
    export default _default;
    import BpmnFactory from "bpmn-js/lib/features/modeling/BpmnFactory";
    import BpmnUpdater from "bpmn-js/lib/features/modeling/BpmnUpdater";
    import ElementFactory from "bpmn-js/lib/features/modeling/ElementFactory";
    import Modeling from "bpmn-js/lib/features/modeling/Modeling";
    import BpmnLayouter from "bpmn-js/lib/features/modeling/BpmnLayouter";
}
declare module "bpmn-js/lib/features/modeling/behavior/AdaptiveLabelPositioningBehavior" {
    /**
     * A component that makes sure that external labels are added
     * together with respective elements and properly updated (DI wise)
     * during move.
     *
     * @param {EventBus} eventBus
     * @param {Modeling} modeling
     */
    function AdaptiveLabelPositioningBehavior(eventBus: any, modeling: any): void;
    namespace AdaptiveLabelPositioningBehavior {
        export const $inject: string[];
    }
    export default AdaptiveLabelPositioningBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/AppendBehavior" {
    function AppendBehavior(eventBus: any, elementFactory: any, bpmnRules: any): void;
    namespace AppendBehavior {
        export const $inject: string[];
    }
    export default AppendBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/AssociationBehavior" {
    function AssociationBehavior(injector: any, modeling: any): void;
    namespace AssociationBehavior {
        export const $inject: string[];
    }
    export default AssociationBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/AttachEventBehavior" {
    /**
     * Replace intermediate event with boundary event when creating or moving results in attached event.
     */
    function AttachEventBehavior(bpmnReplace: any, injector: any): void;
    class AttachEventBehavior {
        /**
         * Replace intermediate event with boundary event when creating or moving results in attached event.
         */
        constructor(bpmnReplace: any, injector: any);
        _bpmnReplace: any;
        replaceShape(shape: any, host: any): any;
    }
    namespace AttachEventBehavior {
        export const $inject: string[];
    }
    export default AttachEventBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/BoundaryEventBehavior" {
    /**
     * BPMN specific boundary event behavior
     */
    function BoundaryEventBehavior(eventBus: any, modeling: any): void;
    namespace BoundaryEventBehavior {
        export const $inject: string[];
    }
    export default BoundaryEventBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/CreateBehavior" {
    function CreateBehavior(injector: any): void;
    namespace CreateBehavior {
        export const $inject: string[];
    }
    export default CreateBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/CreateDataObjectBehavior" {
    /**
     * BPMN specific create data object behavior
     */
    function CreateDataObjectBehavior(eventBus: any, bpmnFactory: any, moddle: any): void;
    namespace CreateDataObjectBehavior {
        export const $inject: string[];
    }
    export default CreateDataObjectBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/CreateParticipantBehavior" {
    /**
     * BPMN-specific behavior for creating participants.
     */
    function CreateParticipantBehavior(canvas: any, eventBus: any, modeling: any): void;
    namespace CreateParticipantBehavior {
        export const $inject: string[];
    }
    export default CreateParticipantBehavior;
    export var PARTICIPANT_BORDER_WIDTH: number;
}
declare module "bpmn-js/lib/features/modeling/behavior/DataInputAssociationBehavior" {
    /**
     * This behavior makes sure we always set a fake
     * DataInputAssociation#targetRef as demanded by the BPMN 2.0
     * XSD schema.
     *
     * The reference is set to a bpmn:Property{ name: '__targetRef_placeholder' }
     * which is created on the fly and cleaned up afterwards if not needed
     * anymore.
     *
     * @param {EventBus} eventBus
     * @param {BpmnFactory} bpmnFactory
     */
    function DataInputAssociationBehavior(eventBus: any, bpmnFactory: any): void;
    namespace DataInputAssociationBehavior {
        export const $inject: string[];
    }
    export default DataInputAssociationBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/DataStoreBehavior" {
    /**
     * BPMN specific data store behavior
     */
    function DataStoreBehavior(canvas: any, commandStack: any, elementRegistry: any, eventBus: any): void;
    namespace DataStoreBehavior {
        export const $inject: string[];
    }
    export default DataStoreBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/DeleteLaneBehavior" {
    /**
     * BPMN specific delete lane behavior
     */
    function DeleteLaneBehavior(eventBus: any, modeling: any, spaceTool: any): void;
    namespace DeleteLaneBehavior {
        export const $inject: string[];
    }
    export default DeleteLaneBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/DetachEventBehavior" {
    /**
     * Replace boundary event with intermediate event when creating or moving results in detached event.
     */
    function DetachEventBehavior(bpmnReplace: any, injector: any): void;
    class DetachEventBehavior {
        /**
         * Replace boundary event with intermediate event when creating or moving results in detached event.
         */
        constructor(bpmnReplace: any, injector: any);
        _bpmnReplace: any;
        replaceShape(shape: any): any;
    }
    namespace DetachEventBehavior {
        export const $inject: string[];
    }
    export default DetachEventBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/DropOnFlowBehavior" {
    function DropOnFlowBehavior(eventBus: any, bpmnRules: any, modeling: any): void;
    namespace DropOnFlowBehavior {
        export const $inject: string[];
    }
    export default DropOnFlowBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/EventBasedGatewayBehavior" {
    function EventBasedGatewayBehavior(eventBus: any, modeling: any): void;
    namespace EventBasedGatewayBehavior {
        export const $inject: string[];
    }
    export default EventBasedGatewayBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/FixHoverBehavior" {
    /**
     * Correct hover targets in certain situations to improve diagram interaction.
     *
     * @param {ElementRegistry} elementRegistry
     * @param {EventBus} eventBus
     * @param {Canvas} canvas
     */
    function FixHoverBehavior(elementRegistry: any, eventBus: any, canvas: any): void;
    namespace FixHoverBehavior {
        export const $inject: string[];
    }
    export default FixHoverBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/GroupBehavior" {
    /**
     * BPMN specific Group behavior
     */
    function GroupBehavior(bpmnFactory: any, canvas: any, elementRegistry: any, eventBus: any, injector: any, moddleCopy: any): void;
    namespace GroupBehavior {
        export const $inject: string[];
    }
    export default GroupBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/ImportDockingFix" {
    /**
     * Fix broken dockings after DI imports.
     *
     * @param {EventBus} eventBus
     */
    function ImportDockingFix(eventBus: any): void;
    namespace ImportDockingFix {
        export const $inject: string[];
    }
    export default ImportDockingFix;
}
declare module "bpmn-js/lib/features/modeling/behavior/IsHorizontalFix" {
    /**
     * A component that makes sure that each created or updated
     * Pool and Lane is assigned an isHorizontal property set to true.
     *
     * @param {EventBus} eventBus
     */
    function IsHorizontalFix(eventBus: any): void;
    namespace IsHorizontalFix {
        export const $inject: string[];
    }
    export default IsHorizontalFix;
}
declare module "bpmn-js/lib/features/modeling/behavior/LabelBehavior" {
    /**
     * A component that makes sure that external labels are added
     * together with respective elements and properly updated (DI wise)
     * during move.
     *
     * @param {EventBus} eventBus
     * @param {Modeling} modeling
     * @param {BpmnFactory} bpmnFactory
     * @param {TextRenderer} textRenderer
     */
    function LabelBehavior(eventBus: any, modeling: any, bpmnFactory: any, textRenderer: any): void;
    namespace LabelBehavior {
        export const $inject: string[];
    }
    export default LabelBehavior;
    /**
     * Calculates a reference point delta relative to a new position
     * of a certain element's bounds
     *
     * @param {Point} point
     * @param {Bounds} oldBounds
     * @param {Bounds} newBounds
     *
     * @return {Delta} delta
     */
    export function getReferencePointDelta(referencePoint: any, oldBounds: any, newBounds: any): any;
    /**
     * Generates the nearest point (reference point) for a given point
     * onto given set of lines
     *
     * @param {Array<Point, Point>} lines
     * @param {Point} point
     *
     * @param {Point}
     */
    export function getReferencePoint(point: any, lines: Map<any, any>): any;
    /**
     * Convert the given bounds to a lines array containing all edges
     *
     * @param {Bounds|Point} bounds
     *
     * @return Array<Point>
     */
    export function asEdges(bounds: any | any): {
        x: any;
        y: any;
    }[][];
}
declare module "bpmn-js/lib/features/modeling/behavior/ModelingFeedback" {
    function ModelingFeedback(eventBus: any, tooltips: any, translate: any): void;
    namespace ModelingFeedback {
        export const $inject: string[];
    }
    export default ModelingFeedback;
}
declare module "bpmn-js/lib/features/modeling/behavior/RemoveElementBehavior" {
    function RemoveElementBehavior(eventBus: any, bpmnRules: any, modeling: any): void;
    namespace RemoveElementBehavior {
        export const $inject: string[];
    }
    export default RemoveElementBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/RemoveParticipantBehavior" {
    /**
     * BPMN specific remove behavior
     */
    function RemoveParticipantBehavior(eventBus: any, modeling: any): void;
    namespace RemoveParticipantBehavior {
        export const $inject: string[];
    }
    export default RemoveParticipantBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/ReplaceConnectionBehavior" {
    function ReplaceConnectionBehavior(eventBus: any, modeling: any, bpmnRules: any, injector: any): void;
    namespace ReplaceConnectionBehavior {
        export const $inject: string[];
    }
    export default ReplaceConnectionBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/ReplaceElementBehaviour" {
    /**
     * BPMN-specific replace behavior.
     */
    function ReplaceElementBehaviour(bpmnReplace: any, bpmnRules: any, elementRegistry: any, injector: any, modeling: any, selection: any): void;
    class ReplaceElementBehaviour {
        /**
         * BPMN-specific replace behavior.
         */
        constructor(bpmnReplace: any, bpmnRules: any, elementRegistry: any, injector: any, modeling: any, selection: any);
        _bpmnReplace: any;
        _elementRegistry: any;
        _selection: any;
        replaceElements(elements: any, newElements: any): void;
    }
    namespace ReplaceElementBehaviour {
        export const $inject: string[];
    }
    export default ReplaceElementBehaviour;
}
declare module "bpmn-js/lib/features/modeling/behavior/ResizeBehavior" {
    /**
     * Set minimum bounds/resize constraints on resize.
     *
     * @param {EventBus} eventBus
     */
    function ResizeBehavior(eventBus: any): void;
    namespace ResizeBehavior {
        export const $inject: string[];
    }
    export default ResizeBehavior;
    export namespace LANE_MIN_DIMENSIONS {
        export const width: number;
        export const height: number;
    }
    export namespace PARTICIPANT_MIN_DIMENSIONS {
        const width_1: number;
        export { width_1 as width };
        const height_1: number;
        export { height_1 as height };
    }
    export namespace SUB_PROCESS_MIN_DIMENSIONS {
        const width_2: number;
        export { width_2 as width };
        const height_2: number;
        export { height_2 as height };
    }
    export namespace TEXT_ANNOTATION_MIN_DIMENSIONS {
        const width_3: number;
        export { width_3 as width };
        const height_3: number;
        export { height_3 as height };
    }
}
declare module "bpmn-js/lib/features/modeling/behavior/ResizeLaneBehavior" {
    /**
     * Invoke {@link Modeling#resizeLane} instead of
     * {@link Modeling#resizeShape} when resizing a Lane
     * or Participant shape.
     */
    function ResizeLaneBehavior(eventBus: any, modeling: any): void;
    namespace ResizeLaneBehavior {
        export const $inject: string[];
    }
    export default ResizeLaneBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/RootElementReferenceBehavior" {
    /**
     * Add referenced root elements (error, escalation, message, signal) if they don't exist.
     * Copy referenced root elements on copy & paste.
     */
    function RootElementReferenceBehavior(bpmnjs: any, eventBus: any, injector: any, moddleCopy: any, bpmnFactory: any): void;
    namespace RootElementReferenceBehavior {
        export const $inject: string[];
    }
    export default RootElementReferenceBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/SpaceToolBehavior" {
    function SpaceToolBehavior(eventBus: any): void;
    namespace SpaceToolBehavior {
        export const $inject: string[];
    }
    export default SpaceToolBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/SubProcessStartEventBehavior" {
    /**
     * Add start event replacing element with expanded sub process.
     *
     * @param {Injector} injector
     * @param {Modeling} modeling
     */
    function SubProcessStartEventBehavior(injector: any, modeling: any): void;
    namespace SubProcessStartEventBehavior {
        export const $inject: string[];
    }
    export default SubProcessStartEventBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/ToggleElementCollapseBehaviour" {
    function ToggleElementCollapseBehaviour(eventBus: any, elementFactory: any, modeling: any, resize: any): void;
    namespace ToggleElementCollapseBehaviour {
        export const $inject: string[];
    }
    export default ToggleElementCollapseBehaviour;
}
declare module "bpmn-js/lib/features/modeling/behavior/UnclaimIdBehavior" {
    /**
     * Unclaims model IDs on element deletion.
     *
     * @param {Canvas} canvas
     * @param {Injector} injector
     * @param {Moddle} moddle
     * @param {Modeling} modeling
     */
    function UnclaimIdBehavior(canvas: any, injector: any, moddle: any, modeling: any): void;
    namespace UnclaimIdBehavior {
        export const $inject: string[];
    }
    export default UnclaimIdBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/UnsetDefaultFlowBehavior" {
    /**
     * A behavior that unsets the Default property of
     * sequence flow source on element delete, if the
     * removed element is the Gateway or Task's default flow.
     *
     * @param {EventBus} eventBus
     * @param {Modeling} modeling
     */
    function DeleteSequenceFlowBehavior(eventBus: any, modeling: any): void;
    namespace DeleteSequenceFlowBehavior {
        export const $inject: string[];
    }
    export default DeleteSequenceFlowBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/UpdateFlowNodeRefsBehavior" {
    /**
     * BPMN specific delete lane behavior
     */
    function UpdateFlowNodeRefsBehavior(eventBus: any, modeling: any, translate: any): void;
    namespace UpdateFlowNodeRefsBehavior {
        export const $inject: string[];
    }
    export default UpdateFlowNodeRefsBehavior;
}
declare module "bpmn-js/lib/features/modeling/behavior/index" {
    namespace _default {
        export const __init__: string[];
        export const adaptiveLabelPositioningBehavior: (string | typeof AdaptiveLabelPositioningBehavior)[];
        export const appendBehavior: (string | typeof AppendBehavior)[];
        export const associationBehavior: (string | typeof AssociationBehavior)[];
        export const attachEventBehavior: (string | typeof AttachEventBehavior)[];
        export const boundaryEventBehavior: (string | typeof BoundaryEventBehavior)[];
        export const rootElementReferenceBehavior: (string | typeof RootElementReferenceBehavior)[];
        export const createBehavior: (string | typeof CreateBehavior)[];
        export const fixHoverBehavior: (string | typeof FixHoverBehavior)[];
        export const createDataObjectBehavior: (string | typeof CreateDataObjectBehavior)[];
        export const createParticipantBehavior: (string | typeof CreateParticipantBehavior)[];
        export const dataInputAssociationBehavior: (string | typeof DataInputAssociationBehavior)[];
        export const dataStoreBehavior: (string | typeof DataStoreBehavior)[];
        export const deleteLaneBehavior: (string | typeof DeleteLaneBehavior)[];
        export const detachEventBehavior: (string | typeof DetachEventBehavior)[];
        export const dropOnFlowBehavior: (string | typeof DropOnFlowBehavior)[];
        export const eventBasedGatewayBehavior: (string | typeof EventBasedGatewayBehavior)[];
        export const groupBehavior: (string | typeof GroupBehavior)[];
        export const importDockingFix: (string | typeof ImportDockingFix)[];
        export const isHorizontalFix: (string | typeof IsHorizontalFix)[];
        export const labelBehavior: (string | typeof LabelBehavior)[];
        export const modelingFeedback: (string | typeof ModelingFeedback)[];
        export const replaceConnectionBehavior: (string | typeof ReplaceConnectionBehavior)[];
        export const removeParticipantBehavior: (string | typeof RemoveParticipantBehavior)[];
        export const replaceElementBehaviour: (string | typeof ReplaceElementBehaviour)[];
        export const resizeBehavior: (string | typeof ResizeBehavior)[];
        export const resizeLaneBehavior: (string | typeof ResizeLaneBehavior)[];
        export const removeElementBehavior: (string | typeof RemoveElementBehavior)[];
        export const toggleElementCollapseBehaviour: (string | typeof ToggleElementCollapseBehaviour)[];
        export const spaceToolBehavior: (string | typeof SpaceToolBehavior)[];
        export const subProcessStartEventBehavior: (string | typeof SubProcessStartEventBehavior)[];
        export const unclaimIdBehavior: (string | typeof UnclaimIdBehavior)[];
        export const updateFlowNodeRefsBehavior: (string | typeof UpdateFlowNodeRefsBehavior)[];
        export const unsetDefaultFlowBehavior: (string | typeof UnsetDefaultFlowBehavior)[];
    }
    export default _default;
    import AdaptiveLabelPositioningBehavior from "bpmn-js/lib/features/modeling/behavior/AdaptiveLabelPositioningBehavior";
    import AppendBehavior from "bpmn-js/lib/features/modeling/behavior/AppendBehavior";
    import AssociationBehavior from "bpmn-js/lib/features/modeling/behavior/AssociationBehavior";
    import AttachEventBehavior from "bpmn-js/lib/features/modeling/behavior/AttachEventBehavior";
    import BoundaryEventBehavior from "bpmn-js/lib/features/modeling/behavior/BoundaryEventBehavior";
    import RootElementReferenceBehavior from "bpmn-js/lib/features/modeling/behavior/RootElementReferenceBehavior";
    import CreateBehavior from "bpmn-js/lib/features/modeling/behavior/CreateBehavior";
    import FixHoverBehavior from "bpmn-js/lib/features/modeling/behavior/FixHoverBehavior";
    import CreateDataObjectBehavior from "bpmn-js/lib/features/modeling/behavior/CreateDataObjectBehavior";
    import CreateParticipantBehavior from "bpmn-js/lib/features/modeling/behavior/CreateParticipantBehavior";
    import DataInputAssociationBehavior from "bpmn-js/lib/features/modeling/behavior/DataInputAssociationBehavior";
    import DataStoreBehavior from "bpmn-js/lib/features/modeling/behavior/DataStoreBehavior";
    import DeleteLaneBehavior from "bpmn-js/lib/features/modeling/behavior/DeleteLaneBehavior";
    import DetachEventBehavior from "bpmn-js/lib/features/modeling/behavior/DetachEventBehavior";
    import DropOnFlowBehavior from "bpmn-js/lib/features/modeling/behavior/DropOnFlowBehavior";
    import EventBasedGatewayBehavior from "bpmn-js/lib/features/modeling/behavior/EventBasedGatewayBehavior";
    import GroupBehavior from "bpmn-js/lib/features/modeling/behavior/GroupBehavior";
    import ImportDockingFix from "bpmn-js/lib/features/modeling/behavior/ImportDockingFix";
    import IsHorizontalFix from "bpmn-js/lib/features/modeling/behavior/IsHorizontalFix";
    import LabelBehavior from "bpmn-js/lib/features/modeling/behavior/LabelBehavior";
    import ModelingFeedback from "bpmn-js/lib/features/modeling/behavior/ModelingFeedback";
    import ReplaceConnectionBehavior from "bpmn-js/lib/features/modeling/behavior/ReplaceConnectionBehavior";
    import RemoveParticipantBehavior from "bpmn-js/lib/features/modeling/behavior/RemoveParticipantBehavior";
    import ReplaceElementBehaviour from "bpmn-js/lib/features/modeling/behavior/ReplaceElementBehaviour";
    import ResizeBehavior from "bpmn-js/lib/features/modeling/behavior/ResizeBehavior";
    import ResizeLaneBehavior from "bpmn-js/lib/features/modeling/behavior/ResizeLaneBehavior";
    import RemoveElementBehavior from "bpmn-js/lib/features/modeling/behavior/RemoveElementBehavior";
    import ToggleElementCollapseBehaviour from "bpmn-js/lib/features/modeling/behavior/ToggleElementCollapseBehaviour";
    import SpaceToolBehavior from "bpmn-js/lib/features/modeling/behavior/SpaceToolBehavior";
    import SubProcessStartEventBehavior from "bpmn-js/lib/features/modeling/behavior/SubProcessStartEventBehavior";
    import UnclaimIdBehavior from "bpmn-js/lib/features/modeling/behavior/UnclaimIdBehavior";
    import UpdateFlowNodeRefsBehavior from "bpmn-js/lib/features/modeling/behavior/UpdateFlowNodeRefsBehavior";
    import UnsetDefaultFlowBehavior from "bpmn-js/lib/features/modeling/behavior/UnsetDefaultFlowBehavior";
}
declare module "bpmn-js/lib/features/modeling/behavior/util/CategoryUtil" {
    /**
     * Creates a new bpmn:CategoryValue inside a new bpmn:Category
     *
     * @param {ModdleElement} definitions
     * @param {BpmnFactory} bpmnFactory
     *
     * @return {ModdleElement} categoryValue.
     */
    export function createCategoryValue(definitions: any, bpmnFactory: any): any;
}
declare module "bpmn-js/lib/features/modeling/behavior/util/GeometricUtil" {
    /**
     * Returns the length of a vector
     *
     * @param {Vector}
     * @return {Float}
     */
    export function vectorLength(v: any): any;
    /**
     * Calculates the angle between a line a the yAxis
     *
     * @param {Array}
     * @return {Float}
     */
    export function getAngle(line: any): any;
    /**
     * Rotates a vector by a given angle
     *
     * @param {Vector}
     * @param {Float} Angle in radians
     * @return {Vector}
     */
    export function rotateVector(vector: any, angle: any): any;
    /**
     * Position of perpendicular foot
     *
     * @param {Point}
     * @param [ {Point}, {Point} ] line defined through two points
     * @return {Point} the perpendicular foot position
     */
    export function perpendicularFoot(point: any, line: any): any;
    /**
     * Calculates the distance between a point and a line
     *
     * @param {Point}
     * @param [ {Point}, {Point} ] line defined through two points
     * @return {Float} distance
     */
    export function getDistancePointLine(point: any, line: any): any;
    /**
     * Calculates the distance between two points
     *
     * @param {Point}
     * @param {Point}
     * @return {Float} distance
     */
    export function getDistancePointPoint(point1: any, point2: any): any;
}
declare module "bpmn-js/lib/features/modeling/behavior/util/LabelLayoutUtil" {
    export function findNewLabelLineStartIndex(oldWaypoints: any, newWaypoints: any, attachment: any, hints: any): any;
    /**
     * Calculate the required adjustment (move delta) for the given label
     * after the connection waypoints got updated.
     *
     * @param {djs.model.Label} label
     * @param {Array<Point>} newWaypoints
     * @param {Array<Point>} oldWaypoints
     * @param {Object} hints
     *
     * @return {Point} delta
     */
    export function getLabelAdjustment(label: any, newWaypoints: Array<any>, oldWaypoints: Array<any>, hints: any): any;
}
declare module "bpmn-js/lib/features/modeling/behavior/util/LineAttachmentUtil" {
    /**
     * Return the attachment of the given point on the specified line.
     *
     * The attachment is either a bendpoint (attached to the given point)
     * or segment (attached to a location on a line segment) attachment:
     *
     * ```javascript
     * var pointAttachment = {
     *   type: 'bendpoint',
     *   bendpointIndex: 3,
     *   position: { x: 10, y: 10 } // the attach point on the line
     * };
     *
     * var segmentAttachment = {
     *   type: 'segment',
     *   segmentIndex: 2,
     *   relativeLocation: 0.31, // attach point location between 0 (at start) and 1 (at end)
     *   position: { x: 10, y: 10 } // the attach point on the line
     * };
     * ```
     *
     * @param {Point} point
     * @param {Array<Point>} line
     *
     * @return {Object} attachment
     */
    export function getAttachment(point: any, line: Array<any>): any;
}
declare module "bpmn-js/lib/features/modeling/behavior/util/LineIntersect" {
    /**
     * Returns the intersection between two line segments a and b.
     *
     * @param {Point} l1s
     * @param {Point} l1e
     * @param {Point} l2s
     * @param {Point} l2e
     *
     * @return {Point}
     */
    export default function lineIntersect(l1s: any, l1e: any, l2s: any, l2e: any): any;
}
declare module "bpmn-js/lib/features/modeling/behavior/util/ResizeUtil" {
    export function getParticipantResizeConstraints(laneShape: any, resizeDirection: any, balanced: any): {
        min: {
            right: any;
            left: number;
        };
        max: {};
    };
}
declare module "bpmn-js/lib/features/modeling/cmd/AddLaneHandler" {
    /**
     * A handler that allows us to add a new lane
     * above or below an existing one.
     *
     * @param {Modeling} modeling
     * @param {SpaceTool} spaceTool
     */
    function AddLaneHandler(modeling: any, spaceTool: any): void;
    class AddLaneHandler {
        /**
         * A handler that allows us to add a new lane
         * above or below an existing one.
         *
         * @param {Modeling} modeling
         * @param {SpaceTool} spaceTool
         */
        constructor(modeling: any, spaceTool: any);
        _modeling: any;
        _spaceTool: any;
        preExecute(context: any): void;
    }
    namespace AddLaneHandler {
        export const $inject: string[];
    }
    export default AddLaneHandler;
}
declare module "bpmn-js/lib/features/modeling/cmd/IdClaimHandler" {
    function IdClaimHandler(moddle: any): void;
    class IdClaimHandler {
        constructor(moddle: any);
        _moddle: any;
        execute(context: any): void;
        revert(context: any): void;
    }
    namespace IdClaimHandler {
        export const $inject: string[];
    }
    export default IdClaimHandler;
}
declare module "bpmn-js/lib/features/modeling/cmd/ResizeLaneHandler" {
    /**
     * A handler that resizes a lane.
     *
     * @param {Modeling} modeling
     */
    function ResizeLaneHandler(modeling: any, spaceTool: any): void;
    class ResizeLaneHandler {
        /**
         * A handler that resizes a lane.
         *
         * @param {Modeling} modeling
         */
        constructor(modeling: any, spaceTool: any);
        _modeling: any;
        _spaceTool: any;
        preExecute(context: any): void;
        resizeBalanced(shape: any, newBounds: any): void;
        resizeSpace(shape: any, newBounds: any): void;
    }
    namespace ResizeLaneHandler {
        export const $inject: string[];
    }
    export default ResizeLaneHandler;
}
declare module "bpmn-js/lib/features/modeling/cmd/SetColorHandler" {
    function SetColorHandler(commandStack: any): void;
    class SetColorHandler {
        constructor(commandStack: any);
        _commandStack: any;
        postExecute(context: any): void;
    }
    namespace SetColorHandler {
        export const $inject: string[];
    }
    export default SetColorHandler;
}
declare module "bpmn-js/lib/features/modeling/cmd/SplitLaneHandler" {
    /**
     * A handler that splits a lane into a number of sub-lanes,
     * creating new sub lanes, if necessary.
     *
     * @param {Modeling} modeling
     */
    function SplitLaneHandler(modeling: any, translate: any): void;
    class SplitLaneHandler {
        /**
         * A handler that splits a lane into a number of sub-lanes,
         * creating new sub lanes, if necessary.
         *
         * @param {Modeling} modeling
         */
        constructor(modeling: any, translate: any);
        _modeling: any;
        _translate: any;
        preExecute(context: any): void;
    }
    namespace SplitLaneHandler {
        export const $inject: string[];
    }
    export default SplitLaneHandler;
}
declare module "bpmn-js/lib/features/modeling/cmd/UpdateCanvasRootHandler" {
    function UpdateCanvasRootHandler(canvas: any, modeling: any): void;
    class UpdateCanvasRootHandler {
        constructor(canvas: any, modeling: any);
        _canvas: any;
        _modeling: any;
        execute(context: any): void;
        revert(context: any): void;
    }
    namespace UpdateCanvasRootHandler {
        export const $inject: string[];
    }
    export default UpdateCanvasRootHandler;
}
declare module "bpmn-js/lib/features/modeling/cmd/UpdateFlowNodeRefsHandler" {
    /**
     * A handler that updates lane refs on changed elements
     */
    function UpdateFlowNodeRefsHandler(elementRegistry: any): void;
    class UpdateFlowNodeRefsHandler {
        /**
         * A handler that updates lane refs on changed elements
         */
        constructor(elementRegistry: any);
        _elementRegistry: any;
        computeUpdates(flowNodeShapes: any, laneShapes: any): any[];
        execute(context: any): void;
        revert(context: any): void;
    }
    namespace UpdateFlowNodeRefsHandler {
        export const $inject: string[];
    }
    export default UpdateFlowNodeRefsHandler;
}
declare module "bpmn-js/lib/features/modeling/cmd/UpdatePropertiesHandler" {
    /**
     * A handler that implements a BPMN 2.0 property update.
     *
     * This should be used to set simple properties on elements with
     * an underlying BPMN business object.
     *
     * Use respective diagram-js provided handlers if you would
     * like to perform automated modeling.
     */
    function UpdatePropertiesHandler(elementRegistry: any, moddle: any, translate: any, modeling: any, textRenderer: any): void;
    class UpdatePropertiesHandler {
        /**
         * A handler that implements a BPMN 2.0 property update.
         *
         * This should be used to set simple properties on elements with
         * an underlying BPMN business object.
         *
         * Use respective diagram-js provided handlers if you would
         * like to perform automated modeling.
         */
        constructor(elementRegistry: any, moddle: any, translate: any, modeling: any, textRenderer: any);
        _elementRegistry: any;
        _moddle: any;
        _translate: any;
        _modeling: any;
        _textRenderer: any;
        execute(context: {
            element: any;
            properties: any;
        }): Array<any>;
        postExecute(context: any): void;
        revert(context: any): any;
    }
    namespace UpdatePropertiesHandler {
        export const $inject: string[];
    }
    export default UpdatePropertiesHandler;
}
declare module "bpmn-js/lib/features/modeling/cmd/UpdateSemanticParentHandler" {
    function UpdateSemanticParentHandler(bpmnUpdater: any): void;
    class UpdateSemanticParentHandler {
        constructor(bpmnUpdater: any);
        _bpmnUpdater: any;
        execute(context: any): void;
        revert(context: any): void;
    }
    namespace UpdateSemanticParentHandler {
        export const $inject: string[];
    }
    export default UpdateSemanticParentHandler;
}
declare module "bpmn-js/lib/features/modeling/util/LaneUtil" {
    /**
     * Collect all lane shapes in the given paren
     *
     * @param  {djs.model.Shape} shape
     * @param  {Array<djs.model.Base>} [collectedShapes]
     *
     * @return {Array<djs.model.Base>}
     */
    export function collectLanes(shape: any, collectedShapes?: Array<any>): Array<any>;
    /**
     * Return the lane children of the given element.
     *
     * @param {djs.model.Shape} shape
     *
     * @return {Array<djs.model.Shape>}
     */
    export function getChildLanes(shape: any): Array<any>;
    /**
     * Return the root element containing the given lane shape
     *
     * @param {djs.model.Shape} shape
     *
     * @return {djs.model.Shape}
     */
    export function getLanesRoot(shape: any): any;
    /**
     * Compute the required resize operations for lanes
     * adjacent to the given shape, assuming it will be
     * resized to the given new bounds.
     *
     * @param {djs.model.Shape} shape
     * @param {Bounds} newBounds
     *
     * @return {Array<Object>}
     */
    export function computeLanesResize(shape: any, newBounds: any): Array<any>;
    export var LANE_INDENTATION: number;
}
declare module "bpmn-js/lib/features/modeling/util/ModelingUtil" {
    /**
     * Return true if element has any of the given types.
     *
     * @param {djs.model.Base} element
     * @param {Array<string>} types
     *
     * @return {boolean}
     */
    export function isAny(element: any, types: Array<string>): boolean;
    /**
     * Return the parent of the element with any of the given types.
     *
     * @param {djs.model.Base} element
     * @param {string|Array<string>} anyType
     *
     * @return {djs.model.Base}
     */
    export function getParent(element: any, anyType: string | Array<string>): any;
}
declare module "bpmn-js/lib/features/ordering/BpmnOrderingProvider" {
    /**
     * a simple ordering provider that makes sure:
     *
     * (0) labels and groups are rendered always on top
     * (1) elements are ordered by a {level} property
     */
    function BpmnOrderingProvider(eventBus: any, canvas: any, translate: any): void;
    class BpmnOrderingProvider {
        /**
         * a simple ordering provider that makes sure:
         *
         * (0) labels and groups are rendered always on top
         * (1) elements are ordered by a {level} property
         */
        constructor(eventBus: any, canvas: any, translate: any);
        getOrdering: (element: any, newParent: any) => {
            parent: any;
            index: number;
        };
    }
    namespace BpmnOrderingProvider {
        export const $inject: string[];
    }
    export default BpmnOrderingProvider;
}
declare module "bpmn-js/lib/features/ordering/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const bpmnOrderingProvider: (string | typeof BpmnOrderingProvider)[];
    }
    export default _default;
    import BpmnOrderingProvider from "bpmn-js/lib/features/ordering/BpmnOrderingProvider";
}
declare module "bpmn-js/lib/features/palette/PaletteProvider" {
    /**
     * A palette provider for BPMN 2.0 elements.
     */
    function PaletteProvider(palette: any, create: any, elementFactory: any, spaceTool: any, lassoTool: any, handTool: any, globalConnect: any, translate: any): void;
    class PaletteProvider {
        /**
         * A palette provider for BPMN 2.0 elements.
         */
        constructor(palette: any, create: any, elementFactory: any, spaceTool: any, lassoTool: any, handTool: any, globalConnect: any, translate: any);
        _palette: any;
        _create: any;
        _elementFactory: any;
        _spaceTool: any;
        _lassoTool: any;
        _handTool: any;
        _globalConnect: any;
        _translate: any;
        getPaletteEntries(element: any): {};
    }
    namespace PaletteProvider {
        export const $inject: string[];
    }
    export default PaletteProvider;
}
declare module "bpmn-js/lib/features/palette/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const paletteProvider: (string | typeof PaletteProvider)[];
    }
    export default _default;
    import PaletteProvider from "bpmn-js/lib/features/palette/PaletteProvider";
}
declare module "bpmn-js/lib/features/popup-menu/ReplaceMenuProvider" {
    /**
     * This module is an element agnostic replace menu provider for the popup menu.
     */
    function ReplaceMenuProvider(popupMenu: any, modeling: any, moddle: any, bpmnReplace: any, rules: any, translate: any): void;
    class ReplaceMenuProvider {
        /**
         * This module is an element agnostic replace menu provider for the popup menu.
         */
        constructor(popupMenu: any, modeling: any, moddle: any, bpmnReplace: any, rules: any, translate: any);
        _popupMenu: any;
        _modeling: any;
        _moddle: any;
        _bpmnReplace: any;
        _rules: any;
        _translate: any;
        register(): void;
        getEntries(element: any): Array<any>;
        getHeaderEntries(element: any): Array<any>;
        _createEntries(element: any, replaceOptions: any): Array<any>;
        _createSequenceFlowEntries(element: any, replaceOptions: any): Array<any>;
        _createMenuEntry(definition: any, element: any, action?: Function): any;
        _getLoopEntries(element: any): Array<any>;
        _getAdHocEntry(element: any): any;
    }
    namespace ReplaceMenuProvider {
        export const $inject: string[];
    }
    export default ReplaceMenuProvider;
}
declare module "bpmn-js/lib/features/popup-menu/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const replaceMenuProvider: (string | typeof ReplaceMenuProvider)[];
    }
    export default _default;
    import ReplaceMenuProvider from "bpmn-js/lib/features/popup-menu/ReplaceMenuProvider";
}
declare module "bpmn-js/lib/features/popup-menu/util/TypeUtil" {
    /**
     * Returns true, if an element is from a different type
     * than a target definition. Takes into account the type,
     * event definition type and triggeredByEvent property.
     *
     * @param {djs.model.Base} element
     *
     * @return {boolean}
     */
    export function isDifferentType(element: any): boolean;
}
declare module "bpmn-js/lib/features/replace/BpmnReplace" {
    /**
     * This module takes care of replacing BPMN elements
     */
    function BpmnReplace(bpmnFactory: any, elementFactory: any, moddleCopy: any, modeling: any, replace: any, selection: any): void;
    class BpmnReplace {
        /**
         * This module takes care of replacing BPMN elements
         */
        constructor(bpmnFactory: any, elementFactory: any, moddleCopy: any, modeling: any, replace: any, selection: any);
        replaceElement: (element: any, target: any, hints?: any) => any;
    }
    namespace BpmnReplace {
        export const $inject: string[];
    }
    export default BpmnReplace;
}
declare module "bpmn-js/lib/features/replace/ReplaceOptions" {
    export var START_EVENT: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType: string;
        };
    })[];
    export var START_EVENT_SUB_PROCESS: {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
        };
    }[];
    export var INTERMEDIATE_EVENT: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType?: undefined;
            eventDefinitionAttrs?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType: string;
            eventDefinitionAttrs?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType: string;
            eventDefinitionAttrs: {
                name: string;
            };
        };
    })[];
    export var END_EVENT: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType: string;
        };
    })[];
    export var GATEWAY: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            instantiate?: undefined;
            eventGatewayType?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            instantiate: boolean;
            eventGatewayType: string;
        };
    })[];
    export var SUBPROCESS_EXPANDED: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            isExpanded: boolean;
            triggeredByEvent?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            triggeredByEvent: boolean;
            isExpanded: boolean;
        };
    })[];
    export var TRANSACTION: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            isExpanded: boolean;
            triggeredByEvent?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            triggeredByEvent: boolean;
            isExpanded: boolean;
        };
    })[];
    export var EVENT_SUB_PROCESS: {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            isExpanded: boolean;
        };
    }[];
    export var TASK: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            isExpanded?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            isExpanded: boolean;
        };
    })[];
    export var BOUNDARY_EVENT: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType: string;
            cancelActivity?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType: string;
            cancelActivity: boolean;
        };
    })[];
    export var EVENT_SUB_PROCESS_START_EVENT: ({
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType: string;
            isInterrupting?: undefined;
        };
    } | {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            eventDefinitionType: string;
            isInterrupting: boolean;
        };
    })[];
    export var SEQUENCE_FLOW: {
        label: string;
        actionName: string;
        className: string;
    }[];
    export var PARTICIPANT: {
        label: string;
        actionName: string;
        className: string;
        target: {
            type: string;
            isExpanded: boolean;
        };
    }[];
}
declare module "bpmn-js/lib/features/replace/index" {
    namespace _default {
        export const __depends__: any[];
        export const bpmnReplace: (string | typeof BpmnReplace)[];
    }
    export default _default;
    import BpmnReplace from "bpmn-js/lib/features/replace/BpmnReplace";
}
declare module "bpmn-js/lib/features/replace-preview/BpmnReplacePreview" {
    function BpmnReplacePreview(eventBus: any, elementRegistry: any, elementFactory: any, canvas: any, previewSupport: any): void;
    namespace BpmnReplacePreview {
        export const $inject: string[];
    }
    export default BpmnReplacePreview;
}
declare module "bpmn-js/lib/features/replace-preview/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const bpmnReplacePreview: (string | typeof BpmnReplacePreview)[];
    }
    export default _default;
    import BpmnReplacePreview from "bpmn-js/lib/features/replace-preview/BpmnReplacePreview";
}
declare module "bpmn-js/lib/features/rules/BpmnRules" {
    /**
     * BPMN specific modeling rule
     */
    function BpmnRules(eventBus: any): void;
    class BpmnRules {
        /**
         * BPMN specific modeling rule
         */
        constructor(eventBus: any);
        init(): void;
        canConnectMessageFlow: typeof canConnectMessageFlow;
        canConnectSequenceFlow: typeof canConnectSequenceFlow;
        canConnectDataAssociation: typeof canConnectDataAssociation;
        canConnectAssociation: typeof canConnectAssociation;
        canMove: typeof canMove;
        canAttach: typeof canAttach;
        canReplace: typeof canReplace;
        canDrop: typeof canDrop;
        canInsert: typeof canInsert;
        canCreate: typeof canCreate;
        canConnect: typeof canConnect;
        canResize: typeof canResize;
        canCopy: typeof canCopy;
    }
    namespace BpmnRules {
        export const $inject: string[];
    }
    export default BpmnRules;
    function canConnectMessageFlow(source: any, target: any): boolean;
    function canConnectSequenceFlow(source: any, target: any): boolean;
    function canConnectDataAssociation(source: any, target: any): false | {
        type: string;
    };
    function canConnectAssociation(source: any, target: any): boolean;
    function canMove(elements: any, target: any): any;
    function canAttach(elements: any, target: any, source: any, position: any): false | "attach";
    /**
     * Defines how to replace elements for a given target.
     *
     * Returns an array containing all elements which will be replaced.
     *
     * @example
     *
     *  [{ id: 'IntermediateEvent_2',
     *     type: 'bpmn:StartEvent'
     *   },
     *   { id: 'IntermediateEvent_5',
     *     type: 'bpmn:EndEvent'
     *   }]
     *
     * @param  {Array} elements
     * @param  {Object} target
     *
     * @return {Object} an object containing all elements which have to be replaced
     */
    function canReplace(elements: any[], target: any, position: any): any;
    /**
     * Can an element be dropped into the target element
     *
     * @return {boolean}
     */
    function canDrop(element: any, target: any, position: any): boolean;
    function canInsert(shape: any, flow: any, position: any): boolean;
    function canCreate(shape: any, target: any, source: any, position: any): boolean;
    function canConnect(source: any, target: any, connection: any): false | {
        type: string;
    };
    function canResize(shape: any, newBounds: any): boolean;
    function canCopy(elements: any, element: any): boolean;
}
declare module "bpmn-js/lib/features/rules/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const bpmnRules: (string | typeof BpmnRules)[];
    }
    export default _default;
    import BpmnRules from "bpmn-js/lib/features/rules/BpmnRules";
}
declare module "bpmn-js/lib/features/search/BpmnSearchProvider" {
    /**
     * Provides ability to search through BPMN elements
     */
    function BpmnSearchProvider(elementRegistry: any, searchPad: any, canvas: any): void;
    class BpmnSearchProvider {
        /**
         * Provides ability to search through BPMN elements
         */
        constructor(elementRegistry: any, searchPad: any, canvas: any);
        _elementRegistry: any;
        _canvas: any;
        find(pattern: string): Array<any>;
    }
    namespace BpmnSearchProvider {
        export const $inject: string[];
    }
    export default BpmnSearchProvider;
}
declare module "bpmn-js/lib/features/search/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const bpmnSearch: (string | typeof BpmnSearchProvider)[];
    }
    export default _default;
    import BpmnSearchProvider from "bpmn-js/lib/features/search/BpmnSearchProvider";
}
declare module "bpmn-js/lib/features/snapping/BpmnConnectSnapping" {
    /**
     * Snap during connect.
     *
     * @param {EventBus} eventBus
     */
    function BpmnConnectSnapping(eventBus: any): void;
    namespace BpmnConnectSnapping {
        export const $inject: string[];
    }
    export default BpmnConnectSnapping;
}
declare module "bpmn-js/lib/features/snapping/BpmnCreateMoveSnapping" {
    /**
     * Snap during create and move.
     *
     * @param {EventBus} eventBus
     * @param {Injector} injector
     */
    function BpmnCreateMoveSnapping(eventBus: any, injector: any): void;
    class BpmnCreateMoveSnapping {
        /**
         * Snap during create and move.
         *
         * @param {EventBus} eventBus
         * @param {Injector} injector
         */
        constructor(eventBus: any, injector: any);
        initSnap(event: any): any;
        addSnapTargetPoints(snapPoints: any, shape: any, target: any): any;
        getSnapTargets(shape: any, target: any): any;
    }
    namespace BpmnCreateMoveSnapping {
        export const $inject: string[];
    }
    export default BpmnCreateMoveSnapping;
}
declare module "bpmn-js/lib/features/snapping/BpmnSnappingUtil" {
    export function getBoundaryAttachment(position: any, targetBounds: any): any;
}
declare module "bpmn-js/lib/features/snapping/index" {
    namespace _default {
        export const __depends__: any[];
        export const __init__: string[];
        export const connectSnapping: (string | typeof BpmnConnectSnapping)[];
        export const createMoveSnapping: (string | typeof BpmnCreateMoveSnapping)[];
    }
    export default _default;
    import BpmnConnectSnapping from "bpmn-js/lib/features/snapping/BpmnConnectSnapping";
    import BpmnCreateMoveSnapping from "bpmn-js/lib/features/snapping/BpmnCreateMoveSnapping";
}
declare module "bpmn-js/lib/import/BpmnImporter" {
    /**
     * An importer that adds bpmn elements to the canvas
     *
     * @param {EventBus} eventBus
     * @param {Canvas} canvas
     * @param {ElementFactory} elementFactory
     * @param {ElementRegistry} elementRegistry
     * @param {Function} translate
     * @param {TextRenderer} textRenderer
     */
    function BpmnImporter(eventBus: any, canvas: any, elementFactory: any, elementRegistry: any, translate: Function, textRenderer: any): void;
    class BpmnImporter {
        /**
         * An importer that adds bpmn elements to the canvas
         *
         * @param {EventBus} eventBus
         * @param {Canvas} canvas
         * @param {ElementFactory} elementFactory
         * @param {ElementRegistry} elementRegistry
         * @param {Function} translate
         * @param {TextRenderer} textRenderer
         */
        constructor(eventBus: any, canvas: any, elementFactory: any, elementRegistry: any, translate: Function, textRenderer: any);
        _eventBus: any;
        _canvas: any;
        _elementFactory: any;
        _elementRegistry: any;
        _translate: Function;
        _textRenderer: any;
        add(semantic: any, parentElement: any): any;
        _attachBoundary(boundarySemantic: any, boundaryElement: any): void;
        addLabel(semantic: any, element: any): any;
        _getEnd(semantic: any, side: any): any;
        _getSource(semantic: any): any;
        _getTarget(semantic: any): any;
        _getElement(semantic: any): any;
    }
    namespace BpmnImporter {
        export const $inject: string[];
    }
    export default BpmnImporter;
}
declare module "bpmn-js/lib/import/BpmnTreeWalker" {
    export default function BpmnTreeWalker(handler: any, translate: any): {
        handleDeferred: () => void;
        handleDefinitions: (definitions: any, diagram?: any) => void;
        handleSubProcess: (subProcess: any, context: any) => void;
        registerDi: (di: any) => void;
    };
}
declare module "bpmn-js/lib/import/Importer" {
    /**
     * The importBpmnDiagram result.
     *
     * @typedef {Object} ImportBPMNDiagramResult
     *
     * @property {Array<string>} warnings
     */
    /**
    * The importBpmnDiagram error.
    *
    * @typedef {Error} ImportBPMNDiagramError
    *
    * @property {Array<string>} warnings
    */
    /**
     * Import the definitions into a diagram.
     *
     * Errors and warnings are reported through the specified callback.
     *
     * @param  {djs.Diagram} diagram
     * @param  {ModdleElement<Definitions>} definitions
     * @param  {ModdleElement<BPMNDiagram>} [bpmnDiagram] the diagram to be rendered
     * (if not provided, the first one will be rendered)
     *
     * Returns {Promise<ImportBPMNDiagramResult, ImportBPMNDiagramError>}
     */
    export function importBpmnDiagram(diagram: any, definitions: any, bpmnDiagram?: any): any;
    /**
     * The importBpmnDiagram result.
     */
    export type ImportBPMNDiagramResult = {
        warnings: Array<string>;
    };
    /**
     * The importBpmnDiagram error.
     */
    export type ImportBPMNDiagramError = Error;
}
declare module "bpmn-js/lib/import/Util" {
    export function elementToString(e: any): string;
}
declare module "bpmn-js/lib/import/index" {
    namespace _default {
        export const __depends__: any[];
        export const bpmnImporter: (string | typeof BpmnImporter)[];
    }
    export default _default;
    import BpmnImporter from "bpmn-js/lib/import/BpmnImporter";
}
declare module "bpmn-js/lib/util/CompatibilityUtil" {
    /**
     * Wraps APIs to check:
     *
     * 1) If a callback is passed -> Warn users about callback deprecation.
     * 2) If Promise class is implemented in current environment.
     *
     * @private
     */
    export function wrapForCompatibility(api: any): (...args: any[]) => any;
}
declare module "bpmn-js/lib/util/DiUtil" {
    export function isExpanded(element: any): boolean;
    export function isInterrupting(element: any): boolean;
    export function isEventSubProcess(element: any): boolean;
    export function hasEventDefinition(element: any, eventType: any): boolean;
    export function hasErrorEventDefinition(element: any): boolean;
    export function hasEscalationEventDefinition(element: any): boolean;
    export function hasCompensateEventDefinition(element: any): boolean;
}
declare module "bpmn-js/lib/util/LabelUtil" {
    /**
     * Returns true if the given semantic has an external label
     *
     * @param {BpmnElement} semantic
     * @return {boolean} true if has label
     */
    export function isLabelExternal(semantic: any): boolean;
    /**
     * Returns true if the given element has an external label
     *
     * @param {djs.model.shape} element
     * @return {boolean} true if has label
     */
    export function hasExternalLabel(element: any): boolean;
    /**
     * Get the position for sequence flow labels
     *
     * @param  {Array<Point>} waypoints
     * @return {Point} the label position
     */
    export function getFlowLabelPosition(waypoints: Array<any>): any;
    /**
     * Get the middle of a number of waypoints
     *
     * @param  {Array<Point>} waypoints
     * @return {Point} the mid point
     */
    export function getWaypointsMid(waypoints: Array<any>): any;
    export function getExternalLabelMid(element: any): any;
    /**
     * Returns the bounds of an elements label, parsed from the elements DI or
     * generated from its bounds.
     *
     * @param {BpmnElement} semantic
     * @param {djs.model.Base} element
     */
    export function getExternalLabelBounds(semantic: any, element: any): {
        x: number;
        y: number;
    } & {
        width: number;
        height: any;
    };
    export function isLabel(element: any): boolean;
    export namespace DEFAULT_LABEL_SIZE {
        export const width: number;
        export const height: number;
    }
    export var FLOW_LABEL_INDENT: number;
}
declare module "bpmn-js/lib/util/ModelUtil" {
    /**
     * Is an element of the given BPMN type?
     *
     * @param  {djs.model.Base|ModdleElement} element
     * @param  {string} type
     *
     * @return {boolean}
     */
    export function is(element: any | any, type: string): boolean;
    /**
     * Return the business object for a given element.
     *
     * @param  {djs.model.Base|ModdleElement} element
     *
     * @return {ModdleElement}
     */
    export function getBusinessObject(element: any | any): any;
}
declare module "bpmn-js/lib/util/PoweredByUtil" {
    export function open(): void;
    export var BPMNIO_IMG: string;
    export var LINK_STYLES: any;
}
