import {ExtendedParameterValue} from '@/lib/BPSim/Parameters/ParameterValue';
import {ResultType} from './ResultRequest';


export interface ParameterInterface {
  parameterName: string;
  value?: ExtendedParameterValue[];
  resultRequest?: ResultType[];
}

export class Parameter implements ParameterInterface {
  parameterName: string;
  value?: ExtendedParameterValue[];
  resultRequest?: ResultType[];

  constructor(
    parameterName: string,
    value?: ExtendedParameterValue[],
    resultRequest?: ResultType[]
  ) {
    this.parameterName = parameterName;
    if (value)
      this.value = value
    if (resultRequest)
      this.resultRequest = resultRequest
  }

  public static fromPartial(parameter: Partial<Parameter>): Parameter {
    if (!parameter.parameterName)
      throw 'Error';

    let p: Parameter = new this(parameter.parameterName);
    if (parameter.value)
      p.value = parameter.value;
    if (parameter.resultRequest)
      p.resultRequest = parameter.resultRequest;
    return p;
  }

  public toJSON(): Parameter {
    if (this.value && this.value.length === 0) {
      delete this.value;
    }
    if (this.resultRequest && this.resultRequest.length === 0) {
      delete this.resultRequest;
    }
    return this;
  }
}
