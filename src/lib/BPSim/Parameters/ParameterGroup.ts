import {Parameter} from './Parameter';
import {ConstantParameters} from './ParameterValue';
import {Property} from './Property';

export enum ParametersGroup {
  TimeParameters = 'TimeParameters',
  ControlParameters = 'ControlParameters',
  CostParameters = 'CostParameters',
  PropertyParameters = 'PropertyParameters',
  PriorityParameters = 'PriorityParameters',
  ResourceParameters = 'ResourceParameters'
}

export class ParameterGroup {
  groupName: ParametersGroup;
  parameters: Array<Property | Parameter>;

  constructor(groupName: ParametersGroup, parameters: Array<Property | Parameter>) {
    this.groupName = groupName;
    this.parameters = parameters;
  }

  public static fromPartial(group: Partial<ParameterGroup>): ParameterGroup {
    if (!group.groupName || !group.parameters) {
      throw 'Error';
    }
    let g: ParameterGroup = new this(group.groupName, new Array<Property | Parameter>());
    for (const parameter of group.parameters) {
      g.parameters.push(Parameter.fromPartial(parameter));
    }
    return g;
  }

}

export enum TimeParameters {
  TransferTime = "TransferTime",
  QueueTime = "QueueTime",
  WaitTime = "WaitTime",
  SetupTime = "SetupTime",
  ProcessingTime = "ProcessingTime",
  ValidationTime = "ValidationTime",
  ReworkTime = "ReworkTime",
  LagTime = "LagTime",
  Duration = "Duration",
  ElapsedTime = "ElapsedTime",
}
export enum ControlParameters {
  InterTriggerTimer = "InterTriggerTimer",
  TriggerCount = "TriggerCount",
  Probability = "Probability",
  Condition = "Condition"
}
export enum CostParameters {
  FixedCost = "FixedCost",
  UnitCost = "UnitCost"
}
export enum PropertyParameters {
  Property = "Property",
  QueueLength = "QueueLength"
}
export enum PriorityParameters {
  Interruptible = "Interruptible",
  Priority = "Priority"
}
export enum ResourceParameters {
  Availability = "Availability",
  Quantity = "Quantity",
  Selection = "Selection",
  Role = "Role"
}

/**
 * Metodo che restituisce una lista 
 */
export function parametersName(groupName: string): string[] {
  let arr = Array<string>();
  switch (groupName as keyof typeof ParametersGroup) {
    case ParametersGroup.TimeParameters:
      arr = Object.keys(TimeParameters);
      break;
    case ParametersGroup.ControlParameters:
      arr = Object.keys(ControlParameters);
      break;
    case ParametersGroup.CostParameters:
      arr = Object.keys(CostParameters);
      break;
    case ParametersGroup.PropertyParameters:
      arr = Object.keys(PropertyParameters);
      break;
    case ParametersGroup.PriorityParameters:
      arr = Object.keys(PriorityParameters);
      break;
    case ParametersGroup.ResourceParameters:
      arr = Object.keys(ResourceParameters);
      break;
    default:
      break;
  }
  return arr;
}

export function controlConst(
  parameterName: keyof typeof ControlParameters
): ConstantParameters[] {
  let arr = new Array<ConstantParameters>();
  switch (parameterName) {
    case ControlParameters.InterTriggerTimer:
      arr = [
        ConstantParameters.NumericParameter,
        ConstantParameters.DurationParameter,
        ConstantParameters.FloatingParameter
      ]
      break;
    case ControlParameters.Condition:
      arr = [ConstantParameters.BooleanParameter];
      break;
    case ControlParameters.Probability:
      arr = [ConstantParameters.NumericParameter, ConstantParameters.FloatingParameter];
      break;
    case ControlParameters.TriggerCount:
      arr = [ConstantParameters.NumericParameter];
      break;
    default:
      break;
  }
  return arr;
}

export function priorityConst(
  parameterName: keyof typeof PriorityParameters
): ConstantParameters[] {
  let arr = new Array<ConstantParameters>();
  if (parameterName === PriorityParameters.Interruptible) {
    arr = [ConstantParameters.BooleanParameter];
  } else if (parameterName === PriorityParameters.Priority) {
    arr = [ConstantParameters.NumericParameter, ConstantParameters.FloatingParameter];
  }
  return arr;
}

export function resourceConst(
  parameterName: keyof typeof ResourceParameters
): ConstantParameters[] {
  let arr = new Array<ConstantParameters>();
  switch (parameterName) {
    case ResourceParameters.Availability:
      arr = [ConstantParameters.BooleanParameter];
      break;
    case ResourceParameters.Quantity:
      arr = [ConstantParameters.NumericParameter];
      break;
    case ResourceParameters.Role:
      arr = [ConstantParameters.StringParameter];
      break;
    case ResourceParameters.Selection:
      arr = [ConstantParameters.NumericParameter, ConstantParameters.StringParameter];
      break;
    default:
      break;
  }
  return arr;
}
