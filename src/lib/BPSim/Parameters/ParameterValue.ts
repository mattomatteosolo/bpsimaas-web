import { TimeUnit } from '@/lib/General/TimeUnit'

export interface ParameterValue {
  validFor?: string;
}

export interface ConstantParameter extends ParameterValue {
  value: number | string | boolean;
}

export interface NumericParameter extends ConstantParameter {
  value: number;
  timeUnit?: TimeUnit;
}

export interface ExtendedParameterValue {
  type: string;
  parameterValue: ParameterValue;
}

export interface ExtendedConstantParameter extends ExtendedParameterValue {
  type: ConstantParameters;
  parameterValue: ConstantParameter;
}

export interface EnumParameter extends ParameterValue {
  values: ExtendedConstantParameter[];
}

export enum ConstantParameters {
  NumericParameter = 'NumericParameter',
  FloatingParameter = 'FloatingParameter',
  DurationParameter = 'DurationParameter',
  DateTimeParameter = 'DateTimeParameter',
  BooleanParameter = 'BooleanParameter',
  StringParameter = 'StringParameter'
}

export enum Distributions {
  beta = 'BetaDistribution',
  truncatedNormal = 'TruncatedNormalDistribution'
}

export interface DistributionParameter extends ParameterValue{
  timeUnit: TimeUnit;
}

export interface ExpressionParameter extends ParameterValue{
  value: string;
}

export type ParameterValues = ConstantParameters | Distributions | "ExpressionParameter" | "EnumParameter";