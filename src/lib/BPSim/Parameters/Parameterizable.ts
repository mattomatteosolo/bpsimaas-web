import { ParametersGroupInterface, ParameterInterface } from './parameters';
import { ParameterizableInterface } from '../../parser';

export class ParametersGroupInterfaceImpl implements ParametersGroupInterface {
  groupName: string;
  parameters = Array<ParameterInterface>();

  constructor(groupName: string, parameters?: ParameterInterface[]) {
    this.groupName = groupName;
    if (parameters) {
      this.parameters = parameters;
    }
  }

  public parameterIndex(parameterName: string): number {
    for (let i = 0; i < this.parameters.length; i++) {
      if (this.parameters[i].parameterName === parameterName) {
        return i;
      }
    }
    return -1;
  }

}

export default class Parameterizable {
  xpathID: string;
  id: string;
  name: string;
  parametersGroup: ParametersGroupInterfaceImpl[]

  /**
   * Costruttore
   * @param node
   * @param element
   */
  constructor(node: Element, element: ParameterizableInterface, parametersGroup?: ParametersGroupInterface[], ) {
    this.xpathID = element.xpathID;
    this.id = node.getAttribute("id") as string;
    this.name = node.getAttribute("name") as string;
    this.parametersGroup = Array<ParametersGroupInterfaceImpl>();
    for (let i = 0; i < element.input.length; i++) {
      const groupName = element.input[i].groupParameter;
      let paramGroup: ParametersGroupInterfaceImpl = new ParametersGroupInterfaceImpl(groupName);
      for (let j = 0; j < element.input[i].parameters.length; j++) {
        let parameter: ParameterInterface = {
          parameterName: element.input[i].parameters[j],
          input: true,
          output: Array<string>(),
        }
        paramGroup.parameters.push(parameter);
      }
      this.parametersGroup.push(paramGroup);
    }
    for (let i = 0; i < element.output.length; i++) {
      const groupName: string = element.output[i].groupParameter;
      const groupIndex: number = this.parameterGroupIndex(groupName);
      if (groupIndex >= 0) {
        for (let j = 0; j < element.output[i].parameters.length; j++) {
          const paramGroup: ParametersGroupInterfaceImpl = this.parametersGroup[groupIndex] as ParametersGroupInterfaceImpl;
          const parameterName: string = element.output[i].parameters[j].parameterName;
          const paramIndex = paramGroup.parameterIndex(parameterName);
          if (paramIndex >= 0) {
            this.parametersGroup[groupIndex].parameters[paramIndex].output = element.output[i].parameters[j].possibleOutputs;
          } else {
            this.parametersGroup[groupIndex].parameters.push({
              parameterName,
              input: false,
              output: element.output[i].parameters[j].possibleOutputs,
            });
          }
        }
      } else {
        const groupName = element.output[i].groupParameter;
        let paramGroup: ParametersGroupInterface = {
          groupName,
          parameters: Array<ParameterInterface>()
        }
        for (let j = 0; j < element.output[i].parameters.length; j++) {
          const parameter: ParameterInterface = {
            parameterName: element.output[i].parameters[j].parameterName,
            input: false,
            output: element.output[i].parameters[j].possibleOutputs,
          }
          paramGroup.parameters.push(parameter);
        }
      }
    }
  }

  /**
   * Metodo che restituisce l'indece del ParametersGroup dato il suo nome
   * @param groupName Nome del ParameterGroup che si vuole ricevere
   * 
   * @returns Indice del ParametersGroup in this.parametersGroup, -1 se non è presente
   * @type number
   */
  parameterGroupIndex(groupName: string): number {
    for (let i = 0; i < this.parametersGroup.length; i++) {
      if (this.parametersGroup[i].groupName === groupName) {
        return i;
      }
    }
    return -1;
  }
}
