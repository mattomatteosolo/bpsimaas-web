import { Parameter } from './Parameter';
import { ExtendedParameterValue } from './ParameterValue';
import { ResultType } from './ResultRequest';

type PropertyType = "string" | "boolean" | "long" | "double" | "duration" | "dateTime";


export class Property extends Parameter {
  propertyName: string;
  propertyType?: PropertyType;

  constructor(
    name: string,
    type?: PropertyType,
    value?: ExtendedParameterValue[],
    resultRequest?: ResultType[]
  ) {
    super("Property", value, resultRequest);
    this.propertyName = name;
    if (this.propertyType)
      this.propertyType = type;
  }
}