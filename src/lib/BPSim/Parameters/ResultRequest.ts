export enum ResultType {
  MIN = 'min',
  MAX = 'sum',
  MEAN = 'mean',
  COUNT = 'count',
  SUM = 'sum'
}
