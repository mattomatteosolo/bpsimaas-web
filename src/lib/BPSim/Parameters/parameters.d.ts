export interface ParameterInterface {
  parameterName: string;
  input: boolean;
  output: Array<string>;
}

export interface ParametersGroupInterface {
  groupName: string;
  parameters: ParameterInterface[];
}