import {ParameterGroup} from '../Parameters/ParameterGroup';

export interface ElementParameterInput {
  parametersGroup: {groupName: string, parameters: any[]}[];
  id: string;
  name: string;
  xpath: string;
}


export class ElementParameter {
  id?: string;
  elementRef: string;
  parametersGroup: ParameterGroup[];

  /**
   * 
   * @param elementRef Stringa che contiene l'id dell'elemento BPMN
   * @param parametersGroup Lista dei ParametersGroup associati a tale elemento
   * @param id id dell'oggetto ElementParameters
   */
  constructor(
    elementRef: string,
    parametersGroup: ParameterGroup[],
    id?: string
  ) {
    this.elementRef = elementRef;
    if (this.id) {
      this.id = id;
    }

    if (parametersGroup.length > 0) this.parametersGroup = parametersGroup;
    else this.parametersGroup = [];
  }

  public static fromPartial(element: Partial<ElementParameter>) {
    if (!element.elementRef || !element.parametersGroup) {
      throw 'Error';
    }
    let e: ElementParameter = new this(
      element.elementRef,
      new Array<ParameterGroup>()
    );

    if (element.id) e.id = element.id;

    for (const group of element.parametersGroup) {
      e.parametersGroup.push(ParameterGroup.fromPartial(group));
    }
    return e;
  }
}
