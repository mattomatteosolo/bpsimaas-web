import {ScenarioInterface} from "./scenario";
import {ElementParameter} from "./ElementParameter";
import ScenarioParameters from "./ScenarioParameters"
import {Calendar} from '../../General/Calendar';

export class Scenario implements ScenarioInterface {
  description?: string;
  created?: string;
  modified?: string;
  author?: string;
  vendor?: string;
  version?: string;
  result?: string;
  inherits?: string;
  elementParameters?: ElementParameter[];
  scenarioParameters?: ScenarioParameters;
  calendars?: Calendar[];
  name: string;
  _id: string;

  constructor(
    name: string,
    id: string,
    others?: Partial<Scenario>
  ) {
    this.name = name;
    this._id = id;
    if (others) {
      if (others.description)
        this.description = others.description;
      if (others.created)
        this.created = others.created;
      if (others.modified)
        this.modified = others.modified;
      if (others.author)
        this.author = others.author;
      if (others.vendor)
        this.vendor = others.vendor;
      if (others.version)
        this.version = others.version;
      if (others.result)
        this.result = others.result;
      if (others.inherits)
        this.inherits = others.inherits;
      if (others.elementParameters) {
        this.elementParameters = new Array<ElementParameter>();
        for (const element of others.elementParameters) {
          this.elementParameters.push(ElementParameter.fromPartial(element))
        }
      }
      if (others.scenarioParameters)
        this.scenarioParameters = others.scenarioParameters;
      if (others.calendars)
        this.calendars = others.calendars;
    }
  }

  /**
   * Metodo che aggiunge un elementParameter a this.elementParameter, creando l'array se questo non esiste
   * 
   * @param elemParameter Oggetto di tipo di ElementParameter da aggiungere a elementParameter
   */
  public addElementParameter(elemParameter: ElementParameter): void {
    if (!this.elementParameters) {
      this.elementParameters = Array<ElementParameter>();
      this.elementParameters.push(elemParameter);
    } else {
      const index: number = this.elementParameterIndex(elemParameter.elementRef)
      if (index >= 0) {
        this.elementParameters.slice(index, 1);
      }
      this.elementParameters.push(elemParameter);
    }
  }

  /**
   * Metodo che restituisce l'indice dell'ElementParameter dato un elementRef, -1 se non presente
   * 
   * @param elementRef Stringa che contiene l'identificativo dell'emento BPMN a cui si riferisce l'ElementParameter
   * @returns number
   */
  public elementParameterIndex(elementRef: string): number {
    if (this.elementParameters) {
      for (let i = 0; i < this.elementParameters.length; i++) {
        if (this.elementParameters[i].elementRef === elementRef) {
          return i;
        }
      }
    }
    return -1;
  }
}
