import { Parameter } from '@/lib/BPSim/Parameters/Parameter';
import { TimeUnit } from '../../General/TimeUnit';

export default interface ScenarioParameters {
  start?: Parameter;
  duration?: Parameter;
  warmup?: Parameter;
  replication: number;
  seed?: number;
  baseTimeUnit?: TimeUnit;
  baseCurrencyUnit?: string;
  baseResutlFrequency?: string;
  baseFrequencyCumul?: boolean;
  traceOutput?: boolean;
  traceFormat?: string;
}