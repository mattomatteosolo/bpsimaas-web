export interface GetModello {
  _id: string;
  nomeModello: string;
}

export interface Modello {
  _id: string;
  nomeModello: string;
  xml: string;
}

export interface GetModelsId {
  modelli: GetModello[];
}