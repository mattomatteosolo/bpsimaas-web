import { ElementParameter } from './ElementParameter';
import ScenarioParameters from './ScenarioParameters';

export interface ScenarioInterface {
  _id: string;
  name: string;
  description?: string;
  created?: string;
  modified?: string;
  author?: string;
  vendor?: string;
  version?: string;
  result?: string;
  inherits?: string;
  elementParameters?: ElementParameter[];
  scenarioParameters?: ScenarioParameters;

  // Methods
  addElementParameter(elemParameter: ElementParameter): void;
  elementParameterIndex(elementRef: string): number;
}

export interface GetScenari {
  _id: string;
  nomeModello: string;
  scenari: ScenarioInterface[];
}
