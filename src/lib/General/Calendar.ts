import {v4 as uuidv4} from 'uuid';
import moment from 'moment';

export class VEvent {
  dtstamp: string = moment.utc().format('YYYYMMDDTHHmmss');
  dtstart: string = moment.utc().format('YYYYMMDDTHHmmss');
  dtend?: string = moment.utc().format('YYYYMMDDTHHmmss');
  rrule: string = "RRULE:FREQ=DAILY;INTERVAL=1";
  uid: string = uuidv4();
}

export class iCalendar {
  prodid: string = "BPSimaaS";
  event: VEvent[] = [new VEvent()];
}

export class Calendar {
  id: string;
  name: string;
  calendar: iCalendar;

  constructor(
    id: string = uuidv4(),
    name: string = "",
    calendar: iCalendar = new iCalendar()
  ) {
    this.id = id;
    this.name = name;
    this.calendar = calendar;
  }

  /**
   * toJSON
   */
  public toJSON(): Partial<Calendar> {
    if (!this.name) {
      throw "Must provide a name for calendar";
    }
    return this;
  }

}
