export const enum RequestStatus {
  LOADING,
  ERROR,
  COMPLETED
}