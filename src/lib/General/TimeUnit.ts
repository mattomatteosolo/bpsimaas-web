export enum TimeUnit {
  none = '',
  MS = 'ms',
  S = 's',
  MIN = 'min',
  H = 'hour',
  D = 'day',
  YEAR = 'year',
}