/**
 * Funzione che dato il contenuto ed il nome di un file, genera un "link" di download
 * @param response risposta ad una chiamata axios
 * @param filename nome del file
 */
function forceFileDownload(response: any, filename: string, mime: string): void {
  const url = window.URL.createObjectURL(new Blob([response], {type: mime}))
  const link = document.createElement('a')
  link.href = url
  link.setAttribute('download', filename) //or any other extension
  document.body.appendChild(link)
  link.click()
}

export {
  forceFileDownload
}
