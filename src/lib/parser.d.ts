export default interface ParserInterface {
  parameterizable: ParameterizableInterface[];
}

export interface ParameterizableInterface {
  xpathID: string;
  input: ParametersGroupInput[];
  output: ParametersGroupOutput[];
}

export interface ParametersGroupInput {
  groupParameter: string;
  parameters: string[];
}

export interface ParametersGroupOutput {
  groupParameter: string;
  parameters: {
    parameterName: string;
    possibleOutputs: string[];
  }[];
}

type PossibleOutput = {
    parameterName: string;
    possibleOutputs: string[];
}
