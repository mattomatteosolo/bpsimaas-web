import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import './assets/sass/index.scss'
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {
  faTrashAlt,
  faPen,
  faProjectDiagram,
  faCaretDown,
  faPlusSquare,
  faTimes
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faTrashAlt,
  faPen,
  faProjectDiagram,
  faCaretDown,
  faPlusSquare,
  faTimes
)

Vue.config.productionTip = false
// Basically using JSON API with little exceptions
axios.defaults.headers!.common['Content-Type'] = 'application/json'
Vue.use(VueAxios, axios)
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
