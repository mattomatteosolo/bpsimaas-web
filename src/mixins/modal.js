const modalMixin = {
  data() {
    return {
      modalActive: false,
    };
  },

  methods: {
    toggleModal() {
      this.modalActive = !this.modalActive;
    },
  },
}

export default modalMixin;