import { Vue, Component } from 'vue-property-decorator';

@Component
export default class ModalMixin extends Vue {
  modalActive: boolean = false;

  /**
   * Metodo che permette di attivare il modal
   */
  public toggleModal(): void {
    this.modalActive = !this.modalActive;
  }
}