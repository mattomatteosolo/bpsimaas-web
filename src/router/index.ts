import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Modello from '../views/Modello.vue'
import Scenario from '../views/Scenario.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    alias: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    props: true
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/models/:idModello',
    name: 'model',
    component: Modello,
    props: true
  },
  {
    path: '/models/:idModello/scenario',
    name: 'createScenario',
    component: Scenario,
    props: true
  },
  {
    path: '/models/:idModello/scenario/:idScenario',
    name: 'editScenario',
    component: Scenario,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(function(to, from, next){
  if (to.name !== 'home' && to.name !== 'about' && to.name !== 'login' && to.name !== 'register')
  {
    // console.log(to);
  }
  // console.log(from);
  // console.log(next);
  next();
})

export default router
