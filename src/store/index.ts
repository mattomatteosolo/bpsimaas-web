import Vue from "vue";
import Vuex from "vuex";


import ApiVuex from './modules/api';
import Request from "./modules/request";
import Login from "./modules/login";
import ModelloVuex from "./modules/modello";
import ParameterizableVuex from "./modules/parameterizable";
import ModelliVuex from "./modules/modelli";
import ScenarioVuex from "./modules/scenario";
import CalendarVuex from './modules/calendar';
import EditorVuex from './modules/editor';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {
    // deleteCalendar(state, payload) {},
    // saveCalendar(state, payload) {},
  },
  actions: {},
  modules: {
    Login,
    Request,
    ModelloVuex,
    ParameterizableVuex,
    ModelliVuex,
    ScenarioVuex,
    ApiVuex,
    CalendarVuex,
    EditorVuex,
  },
  // i getters ricevono lo stato come primo argomento
  getters: {}
})
