import {VuexModule, Module } from 'vuex-module-decorators';

@Module
export default class Api extends VuexModule {
  apiUrl: string = 'http://localhost:5000';

  get getApiUrl(): string {
    return this.apiUrl;
  }
}