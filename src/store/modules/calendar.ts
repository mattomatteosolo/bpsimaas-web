import { Module, VuexModule, Action, Mutation } from "vuex-module-decorators";
import { Calendar } from '@/lib/General/Calendar';

@Module
export default class CalendarVuex extends VuexModule {
  private calendars: Calendar[] = Array<Calendar>();

  @Mutation
  SET_CALENDARS(calendars: Calendar[]) {
    this.calendars = calendars;
  }

  @Mutation
  SET_CALENDAR(payload: { calendar: Calendar, index: number }) {
    if (payload.index !== -1) {
      this.calendars.splice(payload.index, 1, payload.calendar);
    } else {
      this.calendars.push(payload.calendar);
    }
  }

  @Mutation
  DELETE_CALENDAR(payload: number) {
    if (payload !== -1) {
      this.calendars.splice(payload, 1);
    }
  }

  @Action
  saveCalendar(calendar: Calendar) {
    const index: number = this.calendars.findIndex((c: Calendar) => {
      return c.id === calendar.id;
    });
    this.context.commit('SET_CALENDAR', { calendar, index });
  }

  @Action
  deleteCalendar(calendar: Calendar) {
    const index: number = this.calendars.findIndex((c: Calendar) => {
      return c.id === calendar.id;
    });
    this.context.commit('DELETE_CALENDAR', { calendar, index });
  }


  get getCalendars(): Calendar[] {
    return this.calendars;
  }
}