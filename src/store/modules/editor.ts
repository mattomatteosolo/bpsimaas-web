import {
  VuexModule,
  Module,
  Mutation,
  Action
} from 'vuex-module-decorators';
import Vue from 'vue';
import Modeler from 'bpmn-js/lib/Modeler';
import {SaveXMLResult} from 'bpmn-js/lib/BaseViewer';
import {Modello} from '@/lib/BPSim/modello/modello';

@Module
export default class EditorVuex extends VuexModule {
  private _editorOpen = false;
  private _modeler: Modeler | undefined;
  private _newModel = true;

  @Mutation
  public CREATE_MODELER(newModel?: boolean): void {
    this._editorOpen = true;
    this._modeler = Vue.set(this, '_modeler', new Modeler());
    if (newModel !== undefined) this._newModel = newModel;
  }

  @Mutation
  public CLOSE_MODELER() {
    if (!this._modeler) return;
    this._modeler.destroy();
    this._modeler = Vue.set(this, '_modeler', undefined);
    this._editorOpen = false;
  }

  @Mutation
  public ATTACH_MODELER(container: string) {
    if (this._modeler) this._modeler.attachTo(container);
    else console.error('Modeler non disponibile');
  }

  @Mutation
  public OPEN_MODEL(model?: string) {
    if (this._modeler) {
      if (model) {
        this._modeler.importXML(model);
      } else {
        this._modeler.createDiagram();
      }
    } else console.error('Modeler non disponibile');
  }

  @Action
  public async closeModeler() {
    this.context.commit('CLOSE_MODELER');
  }

  @Action
  public async saveModel(fileName?: string) {
    const modeler: Modeler | undefined = this.context.getters['modeler'];
    if (!modeler) {
      console.error("Modeler non attivo");
      return;
    }
    const xmlPromise = modeler.saveXML();
    let xml: SaveXMLResult;
    try {
      xml = await xmlPromise;
    } catch (err) {
      console.error(err);
      return;
    }
    // push to API
    const modello: Modello = this.context.rootGetters['modelloGetter'];
    let method: 'POST' | 'PUT' = 'POST';
    if (!this.context.getters['newModel']) {
      method = 'PUT';
    }
    let url: string = `${this.context.rootGetters["getApiUrl"]}/models`;
    if (method === 'PUT') {
      url += '/' + modello._id;
    }
    let formData = new FormData();
    formData.append(
      'modello',
      new File(
        [xml.xml],
        fileName ? fileName : 'diagram.bpmn',
        {type: 'application/bpmn+xml'}
      )
    );
    const request = Vue.axios({
      method: method,
      url: url,
      data: formData,
      headers: {
        ...this.context.rootGetters["authHeader"],
        "Content-Type": "multipart/form-data",
      },
    });
    try {
      const response = await request;
      this.context.commit("postModelUpdate", {
        _id: response.data.inserted_id,
        nomeModello: response.data.nomeModello,
      });
      this.context.commit("requestCompleted");
    } catch (err) {
      this.context.commit('REQUEST_ERROR');
      console.error(err);
    }
  }

  @Action
  public async saveAsSVG() {
    const modeler: Modeler | undefined = this.context.getters['modeler'];
    if (!modeler) {
      console.error("Modeler non attivo");
      return;
    }
    const svgPromise = modeler.saveSVG();
    try {
      const res = await svgPromise;
      console.log(res.svg);
      // pop download window
    } catch (err) {
      console.error(err);
    }
  }

  get modeler(): Modeler | undefined {
    return this._modeler;
  }

  get editorOpen(): boolean {
    return this._editorOpen;
  }

  get newModel(): boolean {
    return this._newModel;
  }
}
