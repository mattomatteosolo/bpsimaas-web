import {VuexModule, Module, Mutation, Action} from "vuex-module-decorators";
import Vue from 'vue';

@Module
export default class Login extends VuexModule {
  token: string = localStorage.getItem('token') ? localStorage.getItem('token') as string : '';

  @Mutation
  AUTH_SUCCESS(token: string) {
    localStorage.setItem('token', token);
    this.token = token;
  }

  @Mutation
  LOGOUT() {
    localStorage.removeItem('token');
    this.token = '';
  }

  @Action
  logout() {
    this.context.commit('LOGOUT');
  }

  @Action
  async login(payload: {email: string, password: string}) {
    const url = `${this.context.getters['getApiUrl']}/login`
    const request = Vue.axios.post(
      url,
      {
        email: payload.email,
        password: payload.password
      },
    );
    this.context.commit('REQUEST_PENDING');
    try {
      const response = await request;
      this.context.commit('REQUEST_COMPLETED');
      this.context.commit('AUTH_SUCCESS', response.data.access);
    }
    catch (err) {
      this.context.commit('REQUEST_ERROR');
    }
  }

  get authHeader() {
    return {
      Authorization: 'Bearer ' + this.token
    }
  }

  get isLogged() {
    return !!this.token;
  }
}
