import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { GetModello, GetModelsId } from '@/lib/BPSim/modello/modello';
import { AxiosResponse } from 'axios';
import Vue from 'vue';


@Module
export default class ModelliVuex extends VuexModule {
  modelli: GetModello[] = Array<GetModello>();


  @Mutation
  public postModelUpdate(paylod: GetModello): void {
    this.modelli.push(paylod);
  }

  @Mutation
  public deleteModelSuccess(payload: { _id: string }): void {
    for (let i = 0; i < this.modelli.length; i++) {
      if (this.modelli[i]._id === payload._id) {
        this.modelli.splice(i, 1);
        break;
      }
    }
  }

  @Mutation
  requestModelsCompleted(payload: GetModelsId) {
    this.modelli = payload.modelli;
  }

  @Action
  async downloadModels() {
    const request: Promise<AxiosResponse<GetModelsId>> = Vue.axios.get(this.context.rootGetters['getApiUrl'] + '/models', { headers: this.context.rootGetters.authHeader });
    this.context.commit('pendingRequest');
    try {
      const response: AxiosResponse<GetModelsId> = await request;
      this.context.commit('requestModelsCompleted', response.data);
      this.context.commit('requestCompleted');
    }
    catch (err) {
      this.context.commit('requestError')
    }
  }

  get getModels(): GetModello[] {
    return this.modelli;
  }
}