import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import Vue from 'vue';
import { AxiosResponse } from 'axios';
import _ from 'lodash';
import { GetScenari, ScenarioInterface } from '@/lib/BPSim/modello/scenario';
import { Modello } from '@/lib/BPSim/modello/modello';
import Parameterizable from '@/lib/BPSim/Parameters/Parameterizable';
import ParserInterface from '@/lib/parser';


@Module
export default class ModelloModule extends VuexModule {
  modello: Modello | undefined;
  scenari: ScenarioInterface[] | undefined;

  @Mutation
  SET_MODEL(payload: { modello: Modello, scenari: ScenarioInterface[] }): void {
    this.modello = Vue.set(this, 'modello', payload.modello);
    this.scenari = Vue.set(this, 'scenari', payload.scenari);
  }

  @Mutation
  PUSH_SCENARIO(scenario: ScenarioInterface) {
    // Se lo scenario è in this.scenari, lo rimpiazzi altrimenti lo "pushi"
    if (this.scenari) {
      const idx = this.scenari.findIndex((sc: ScenarioInterface) => sc._id === scenario._id);
      if (idx >= 0) {
        Vue.set(this.scenari, idx, scenario);
      } else {
        this.scenari = Vue.set(this, 'scenari', [scenario, ...this.scenari]);
      }
    } else {
      this.scenari = Vue.set(this, 'scenari', [scenario]);
    }
  }

  @Action
  public async getModel(payload: { idModello: string }): Promise<void> {
    const requestModello: Promise<AxiosResponse<string>> = Vue.axios.get(
      `${this.context.getters['getApiUrl']}/models/${payload.idModello}`,
      {
        headers: { ...this.context.rootGetters.authHeader }
      });
    const requestScenari: Promise<AxiosResponse<GetScenari>> = Vue.axios.get(
      `${this.context.getters['getApiUrl']}/models/${payload.idModello}/scenario`,
      {
        headers: { ...this.context.rootGetters.authHeader }
      });

    this.context.commit('REQUEST_PENDING');
    try {
      const responseModello: AxiosResponse<string> = await requestModello;
      const responseScenari: AxiosResponse<GetScenari> = await requestScenari;
      const modello: Modello = {
        _id: responseScenari.data._id,
        nomeModello: responseScenari.data.nomeModello,
        xml: responseModello.data
      };
      const scenari: ScenarioInterface[] = responseScenari.data.scenari;
      this.context.commit('SET_MODEL', {
        modello,
        scenari
      });
      this.context.commit("REQUEST_COMPLETED");
    }
    catch (error) {
      this.context.commit('REQUEST_ERROR');
      console.log(error.response);
    }
  }

  @Action
  public async getParameterizable(idModello: string): Promise<void> {
    if (this.modello === undefined) {
      console.error("Error: No model found");
    }
    const request: Promise<AxiosResponse<ParserInterface>> = Vue.axios.get(
      `${this.context.getters['getApiUrl']}/models/${idModello}/parser`,
      {
        headers: this.context.rootGetters.authHeader
      }
    );
    this.context.commit('REQUEST_PENDING');
    try {
      const response: AxiosResponse<ParserInterface> = await request;
      let elements: Parameterizable[] = [];
      let domParser = new DOMParser();
      let modelloXML = domParser.parseFromString(
        this.modello!.xml,
        "application/xml"
      );
      response.data.parameterizable.forEach(element => {
        let data: Element = modelloXML
          .evaluate(element.xpathID, modelloXML, null, XPathResult.ANY_TYPE, null)
          .iterateNext() as Element;
        if (data === null) {
          throw "Error: received no valid data for parameterizable";
        }
        const elem: Parameterizable = new Parameterizable(data, element);
        elements.push(elem);
      });
      this.context.commit('REQUEST_COMPLETED');
      this.context.commit("SET_PARAMETERIZABLE", { parameterizable: elements });
    } catch (err) {
      this.context.commit('REQUEST_ERROR');
      console.error(err);
    }
  }

  @Action
  public async downloadModello(payload: { idModello: string, scenari: string[] }): Promise<void | string> {
    const url: string = `${this.context.getters['getApiUrl']}/models/${payload.idModello}/download`;
    const headers = {
      ...this.context.getters['authHeader'],
      'Content-type': 'application/json'
    };
    const data = JSON.stringify({ scenari: payload.scenari })
    const request: Promise<AxiosResponse<string>> = Vue.axios({
      method: 'post',
      url,
      headers,
      data
    });
    this.context.commit('REQUEST_PENDING');
    try {
      const response = await request;
      this.context.commit('REQUEST_COMPLETED');
      return response.data;
    } catch (error) {
      this.context.commit('REQUEST_ERROR');
      console.error(error);
      console.error(error.response.data);
    }
    return;
  }

  get modelloGetter(): Modello | undefined {
    return this.modello;
  }

  get getScenari(): ScenarioInterface[] | undefined {
    return this.scenari;
  }
}
