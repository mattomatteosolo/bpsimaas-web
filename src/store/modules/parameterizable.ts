import { VuexModule, Module, Mutation } from "vuex-module-decorators";
import Parameterizable from '@/lib/BPSim/Parameters/Parameterizable';


@Module
export default class ParmeterizableVuex extends VuexModule {
  parameterizable: Parameterizable[] = Array<Parameterizable>();

  @Mutation
  SET_PARAMETERIZABLE(payload: { parameterizable: Parameterizable[] }) {
    this.parameterizable = payload.parameterizable;
  }

  @Mutation
  removeParameterizable() {
    this.parameterizable = [];
  }

  get getParameterizable() {
    return this.parameterizable;
  }
}
