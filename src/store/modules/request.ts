import { Module, Mutation, VuexModule } from "vuex-module-decorators";
import { RequestStatus } from '@/lib/General/RequestStatus';

@Module
export default class RequestVuex extends VuexModule{
  status = RequestStatus.COMPLETED;

  @Mutation
  requestError() {
    this.status = RequestStatus.ERROR;
  }

  @Mutation
  REQUEST_ERROR() {
    this.status = RequestStatus.ERROR;
  }

  @Mutation
  pendingRequest() {
    this.status = RequestStatus.LOADING;
  }
  @Mutation
  REQUEST_PENDING() {
    this.status = RequestStatus.LOADING;
  }

  @Mutation
  requestCompleted() {
    this.status = RequestStatus.COMPLETED;
  }

  @Mutation
  REQUEST_COMPLETED() {
    this.status = RequestStatus.COMPLETED;
  }

  get getStatus(): RequestStatus {
    return this.status;
  }
}