import {VuexModule, Module, Action, Mutation} from "vuex-module-decorators";
import {Scenario} from '@/lib/BPSim/modello/ScenarioImplementation';
import Vue from 'vue';
import {AxiosResponse} from 'axios';

@Module
export default class ScenarioVuex extends VuexModule {
  scenario: Scenario | undefined;

  @Mutation
  SET_SCENARIO(payload: Scenario) {
    this.scenario = Vue.set(this, 'scenario', payload);
  }

  /**
   * Metodo Action, che si occupa di recuperare lo scenario dato l'id del modello a cui appartiene e il suo stesso id
   * 
   * @param payload Oggetto che contiene id del modello e dello scenario
   */
  @Action
  public async downloadScenario(
    payload: {idModello: string, idScenario: string}
  ): Promise<void> {
    // Richisesta asincrona
    const request: Promise<AxiosResponse<{scenario: any}>> = Vue.axios.get(
      this.context.getters['getApiUrl'] + '/models/' + payload.idModello + '/scenario/' + payload.idScenario,
      {
        headers: {
          ...this.context.getters['authHeader']
        }
      });
    // Setting request come pending
    this.context.commit('REQUEST_PENDING');

    // Risoluzione asincrona
    try {
      const response: Scenario = (await request).data.scenario;
      this.context.commit('REQUEST_COMPLETED');
      this.context.commit('SET_SCENARIO', response);
      if (response.calendars)
        this.context.commit("SET_CALENDARS", response.calendars);
    } catch (error) {
      this.context.dispatch('REQUEST_ERROR');
      console.error(error.response);
    }
  }

  /**
   * Metodo che permette di caricare uno scenario sulle api, effettuando una POST
   * o una PUT a seconda di qunato specificato
   * @param payload oggetto che contiene l'azione da intraprendere ['PUT','POST'],
   * l'id del modello e lo scenario da caricare
   */
  @Action
  public async pushScenario(payload: {action: string, idModello: string, scenario: Scenario}) {
    let action: Function;
    let url: string = `${this.context.getters['getApiUrl']}/models/${payload.idModello}/scenario`;
    // Switch to select proper action
    switch (payload.action) {
      case 'POST':
        action = Vue.axios.post;
        break;

      case 'PUT':
        action = Vue.axios.put;
        url += `/${payload.scenario._id}`;
        break;

      default:
        action = Vue.axios.post
        break;
    }
    const header = {
      ...this.context.getters['authHeader'],
      'Content-type': 'application/json'
    }
    const request: Promise<AxiosResponse<{msg: string}>> = action(
      url,
      payload.scenario,
      {
        headers: header
      }
    );
    this.context.commit('REQUEST_PENDING');

    try {
      await request;
      this.context.commit('REQUEST_COMPLETED');
      this.context.commit('PUSH_SCENARIO', {
        _id: payload.scenario._id,
        name: payload.scenario.name
      });
    } catch (e) {
      console.error(e);
      this.context.commit('REQUEST_ERROR');
    }
  }

  @Action
  public async deleteScenario(payload: {idModello: string, idScenario: string}): Promise<void> {
    const {idModello, idScenario} = payload;
    let url: string = `${this.context.getters['getApiUrl']}/models/${idModello}/scenario/${idScenario}`;

    const header = {
      ...this.context.getters['authHeader'],
      'Content-type': 'application/json'
    }
    const request: Promise<AxiosResponse<{msg: string}>> = Vue.axios.delete(
      url,
      {
        headers: header
      }
    );
    this.context.commit('REQUEST_PENDING');

    try {
      await request;
      this.context.commit('REQUEST_COMPLETED');
      await this.context.dispatch('getModel', {idModello});
    } catch(err) {
      this.context.commit('REQUEST_ERROR');
      return err.response;
    }
  }

  get getScenario(): Scenario | undefined {
    return this.scenario;
  }
}
